#!/bin/bash

function usage() {
  cat <<EOM
DESCRIPTION:

  Creates a topic in a broker.

USAGE: $0 topicName

  topicName - topic name you want to create

EOM
  exit 1
}

if [[ $# == 0 ]]; then usage; fi

TOPIC_NAME = $1
docker exec kafka kafka-topics --zookeeper zookeeper:2181 --create --topic $TOPIC_NAME --partitions 8 --replication-factor 1