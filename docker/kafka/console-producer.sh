#!/bin/bash

function usage() {
  cat <<EOM
USAGE: $0 <topic>
EOM
  exit 1
}

if [[ $# == 0 ]]; then
  usage
fi

CONTAINER_NAME=$(docker ps --format "{{.Names}}" --filter status=running | grep kafka)
TOPIC=$1

docker exec -it $CONTAINER_NAME kafka-console-producer --broker-list localhost:9092 --topic $TOPIC --sync
