package com.pcb.feedproducer.feed.query;

public class FeedQueryUtil {
    public static final String POSTGRES_SELECT_FEED_DETAILS_QUERY =
            "SELECT\n" +
                    "  a.ACCOUNT_NO as customerAcctNumber,\n" +
                    "  a.PRODUCT_UID as productId,\n" +
                    "  a.INACTIVE_CYCLES as numberOfCyclesInactive,\n" +
                    "  a.OPEN_DT as openDate,\n" +
                    "  AM00_LIMIT_CREDIT as limitCredit,\n" +
                    "  AM00_STATC_CURRENT_PAST_DUE as statcCurrentPastDue,\n" +
                    "  AM00_CUSTOM_DATA_81 as customData81,\n" +
                    "  AM00_DATE_LAST_BILLING as dateLastBilling,\n" +
                    "  AM00_STATC_CLOSED as statcClosed,\n" +
                    "  AM00_STATC_SECURITY_FRAUD as statcSecurityFraud,\n" +
                    "  AM00_BANKRUPTCY_TYPE as bankruptcyType,\n" +
                    "  AM00_STATC_CREDIT_REVOKED as statcCreditRevoked,\n" +
                    "  AM00_STATC_CHARGEOFF as statcChargeoff,\n" +
                    "  AM00_STATF_REINSTATE_CHARGEOFF as statfReinstateChargeoff,\n" +
                    "  AM00_STATC_CURRENT_OVERLIMIT as statcCurrentOverlimit,\n" +
                    "  AM00_STATC_WATCH as statcWatch,\n" +
                    "  AM00_STATC_TIERED_AUTH as statcTieredAuth,\n" +
                    "  AM00_STATF_CREDIT_COUNSELING as statfCreditCounseling,\n" +
                    "  AM00_STATF_CRV as statfCrv,\n" +
                    "  AM01_CARD_RECEIPT_VERIFY_IND as cardReceiptVerifyInd,\n" +
                    "  AM01_DATE_LAST_PIN_CHANGE as dateLastPinChange,\n" +
                    "  AM01_DATE_ACCOUNT_VALID_FROM as dateAccountValidFrom,\n" +
                    "  AM01_DATE_LAST_CARD_REQUEST as dateLastCardRequest,\n" +
                    "  AM01_REASON_LAST_CARD_REQUEST as reasonLastCardRequest,\n" +
                    "  AM02_BALANCE_CURRENT as balanceCurrent,\n" +
                    "  AM04_DELINQUENT_AMOUNT as delinquentAmount,\n" +
                    "  AM0A_STATC_CARD_WATCH as statcCardWatch,\n" +
                    "  AM0E_NAME_EMBOSS as nameEmboss,\n" +
                    "  (\n" +
                    "    SELECT\n" +
                    "      ci.CUSTOMER_IDENTIFIER_NO\n" +
                    "    FROM\n" +
                    "      ACCOUNT_CUSTOMER ac\n" +
                    "      INNER JOIN CUSTOMER c on ac.CUSTOMER_UID = c.CUSTOMER_UID\n" +
                    "      INNER JOIN CUSTOMER_IDENTIFIER ci on c.CUSTOMER_UID = ci.CUSTOMER_UID\n" +
                    "    WHERE\n" +
                    "      ac.ACCOUNT_UID = a.ACCOUNT_UID\n" +
                    "      AND ac.ACCOUNT_CUSTOMER_ROLE_UID = 1\n" +
                    "      AND ac.ACTIVE_IND = 'Y'\n" +
                    "      AND ci.TYPE = 'PCF-CUSTOMER-ID'\n" +
                    "      AND ci.DISABLED_IND = 'N'\n" +
                    "  ) as customerIdFromHeader,\n" +
                    "  (\n" +
                    "    SELECT\n" +
                    "      ci.CUSTOMER_IDENTIFIER_NO\n" +
                    "    FROM\n" +
                    "      ACCOUNT_CUSTOMER ac\n" +
                    "      INNER JOIN CUSTOMER c on ac.CUSTOMER_UID = c.CUSTOMER_UID\n" +
                    "      INNER JOIN CUSTOMER_IDENTIFIER ci on c.CUSTOMER_UID = ci.CUSTOMER_UID\n" +
                    "    WHERE\n" +
                    "      ac.ACCOUNT_UID = a.ACCOUNT_UID\n" +
                    "      AND ac.ACCOUNT_CUSTOMER_ROLE_UID = 2\n" +
                    "      AND ac.ACTIVE_IND = 'Y'\n" +
                    "      AND ci.TYPE = 'PCF-CUSTOMER-ID'\n" +
                    "      AND ci.DISABLED_IND = 'N'\n" +
                    "    LIMIT\n" +
                    "      1\n" +
                    "  ) as jointCustomerId,\n" +
                    "  (\n" +
                    "    SELECT\n" +
                    "      COUNT(*)\n" +
                    "    FROM\n" +
                    "      ACCESS_MEDIUM am\n" +
                    "      INNER JOIN ACCOUNT_CUSTOMER ac on am.ACCOUNT_CUSTOMER_UID = ac.ACCOUNT_CUSTOMER_UID\n" +
                    "    WHERE\n" +
                    "      ac.ACCOUNT_UID = a.ACCOUNT_UID\n" +
                    "      AND (\n" +
                    "        am.DEACTIVATED_DT IS NULL\n" +
                    "        OR am.DEACTIVATED_DT > NOW()\n" +
                    "      )\n" +
                    "      AND (\n" +
                    "        am.EXPIRY_DT IS NULL\n" +
                    "        OR am.EXPIRY_DT > NOW()\n" +
                    "      )\n" +
                    "  ) as numberOfPaymentIds,\n" +
                    "  (\n" +
                    "    SELECT\n" +
                    "      COUNT(*)\n" +
                    "    FROM\n" +
                    "      ACCOUNT_CUSTOMER ac\n" +
                    "      INNER JOIN CUSTOMER c on ac.CUSTOMER_UID = c.CUSTOMER_UID\n" +
                    "      INNER JOIN CUSTOMER_IDENTIFIER ci on c.CUSTOMER_UID = ci.CUSTOMER_UID\n" +
                    "    WHERE\n" +
                    "      ac.ACCOUNT_UID = a.ACCOUNT_UID\n" +
                    "      AND ac.ACCOUNT_CUSTOMER_ROLE_UID = 2\n" +
                    "      AND ac.ACTIVE_IND = 'Y'\n" +
                    "      AND ci.TYPE = 'PCF-CUSTOMER-ID'\n" +
                    "      AND ci.DISABLED_IND = 'N'\n" +
                    "  ) as numberOfAuthorizedUsers,\n" +
                    "  (\n" +
                    "    SELECT\n" +
                    "      iar.ACTIVE_IND\n" +
                    "    FROM\n" +
                    "      INTERAC_AUTODEP_REGISTER iar\n" +
                    "    WHERE\n" +
                    "      iar.ACCOUNT_UID = a.ACCOUNT_UID\n" +
                    "  ) as userIndicator01,\n" +
                    "  (\n" +
                    "    SELECT\n" +
                    "      ai.ACCOUNT_IDENTIFIER_NO\n" +
                    "    FROM\n" +
                    "      ACCOUNT_IDENTIFIER ai\n" +
                    "    WHERE\n" +
                    "      ai.ACCOUNT_UID = a.ACCOUNT_UID\n" +
                    "      AND ai.TYPE = 'PCB'\n" +
                    "    LIMIT\n" +
                    "      1\n" +
                    "  ) as accountIdentifierNo\n" +
                    "FROM\n" +
                    "  ACCOUNT a\n" +
                    "  INNER JOIN (\n" +
                    "    SELECT\n" +
                    "      SUBSTRING (MAST_ACCOUNT_ID, 2) as MAST_ACCOUNT_ID,\n" +
                    "      AM00_LIMIT_CREDIT,\n" +
                    "      AM00_STATC_CURRENT_PAST_DUE,\n" +
                    "      AM00_CUSTOM_DATA_81,\n" +
                    "      AM00_DATE_LAST_BILLING,\n" +
                    "      AM00_STATC_CLOSED,\n" +
                    "      AM00_STATC_SECURITY_FRAUD,\n" +
                    "      AM00_BANKRUPTCY_TYPE,\n" +
                    "      AM00_STATC_CREDIT_REVOKED,\n" +
                    "      AM00_STATC_CHARGEOFF,\n" +
                    "      AM00_STATF_REINSTATE_CHARGEOFF,\n" +
                    "      AM00_STATC_CURRENT_OVERLIMIT,\n" +
                    "      AM00_STATC_WATCH,\n" +
                    "      AM00_STATC_TIERED_AUTH,\n" +
                    "      AM00_STATF_CREDIT_COUNSELING,\n" +
                    "      AM00_STATF_CRV\n" +
                    "    FROM\n" +
                    "      TSYS_PCB_MC_ACCTMASTER_DAILY_AM00\n" +
                    "  ) as am00 ON am00.MAST_ACCOUNT_ID = a.ACCOUNT_NO\n" +
                    "  INNER JOIN (\n" +
                    "    SELECT\n" +
                    "      SUBSTRING (MAST_ACCOUNT_ID, 2) as MAST_ACCOUNT_ID,\n" +
                    "      AM01_CUSTOMER_ID,\n" +
                    "      AM01_ACCOUNT_NUM,\n" +
                    "      AM01_CARD_RECEIPT_VERIFY_IND,\n" +
                    "      AM01_DATE_LAST_PIN_CHANGE,\n" +
                    "      AM01_DATE_ACCOUNT_VALID_FROM,\n" +
                    "      AM01_DATE_LAST_CARD_REQUEST,\n" +
                    "      AM01_REASON_LAST_CARD_REQUEST\n" +
                    "    FROM\n" +
                    "      TSYS_PCB_MC_ACCTMASTER_DAILY_AM01\n" +
                    "  ) as am01 ON am01.MAST_ACCOUNT_ID = a.ACCOUNT_NO\n" +
                    "  INNER JOIN (\n" +
                    "    SELECT\n" +
                    "      SUBSTRING (MAST_ACCOUNT_ID, 2) as MAST_ACCOUNT_ID,\n" +
                    "      AM02_BALANCE_CURRENT\n" +
                    "    FROM\n" +
                    "      TSYS_PCB_MC_ACCTMASTER_DAILY_AM02\n" +
                    "  ) as am02 ON am02.MAST_ACCOUNT_ID = a.ACCOUNT_NO\n" +
                    "  INNER JOIN (\n" +
                    "    SELECT\n" +
                    "      SUBSTRING (MAST_ACCOUNT_ID, 2) as MAST_ACCOUNT_ID,\n" +
                    "      SUM (\n" +
                    "        CAST (AM04_AMT_PASTDUE_1 AS DECIMAL) + CAST (AM04_AMT_PASTDUE_2 AS DECIMAL) + CAST (AM04_AMT_PASTDUE_3 AS DECIMAL) + CAST (AM04_AMT_PASTDUE_4 AS DECIMAL) + CAST (AM04_AMT_PASTDUE_5 AS DECIMAL) + CAST (AM04_AMT_PASTDUE_6 AS DECIMAL) + CAST (AM04_AMT_PASTDUE_7 AS DECIMAL) + CAST (AM04_AMT_PASTDUE_8 AS DECIMAL) + CAST (AM04_AMT_PASTDUE_9 AS DECIMAL) + CAST (AM04_AMT_PASTDUE_10 AS DECIMAL) + CAST (AM04_AMT_PASTDUE_11 AS DECIMAL) + CAST (AM04_AMT_PASTDUE_12 AS DECIMAL)\n" +
                    "      ) as AM04_DELINQUENT_AMOUNT\n" +
                    "    FROM\n" +
                    "      TSYS_PCB_MC_ACCTMASTER_DAILY_AM04\n" +
                    "    GROUP BY\n" +
                    "      MAST_ACCOUNT_ID\n" +
                    "  ) as am04 ON am04.MAST_ACCOUNT_ID = a.ACCOUNT_NO\n" +
                    "  INNER JOIN (\n" +
                    "    SELECT\n" +
                    "      SUBSTRING (MAST_ACCOUNT_ID, 2) as MAST_ACCOUNT_ID,\n" +
                    "      AM0A_STATC_CARD_WATCH\n" +
                    "    FROM\n" +
                    "      TSYS_PCB_MC_ACCTMASTER_DAILY_AM0A\n" +
                    "  ) as am0a ON am0a.MAST_ACCOUNT_ID = a.ACCOUNT_NO\n" +
                    "  INNER JOIN (\n" +
                    "    SELECT\n" +
                    "      SUBSTRING (MAST_ACCOUNT_ID, 2) as MAST_ACCOUNT_ID,\n" +
                    "      AM0E_NAME_EMBOSS\n" +
                    "    FROM\n" +
                    "      TSYS_PCB_MC_ACCTMASTER_DAILY_AM0E\n" +
                    "  ) as am0e ON am0e.MAST_ACCOUNT_ID = a.ACCOUNT_NO\n" +
                    "  INNER JOIN ACCOUNT_CUSTOMER ac ON a.ACCOUNT_UID = ac.ACCOUNT_UID";

    public static final String POSTGRES_GROUP_BY_CLAUSE =
            "GROUP BY\n" +
                    "  customerAcctNumber,\n" +
                    "  productId,\n" +
                    "  numberOfCyclesInactive,\n" +
                    "  openDate,\n" +
                    "  limitCredit,\n" +
                    "  statcCurrentPastDue,\n" +
                    "  customData81,\n" +
                    "  dateLastBilling,\n" +
                    "  statcClosed,\n" +
                    "  statcSecurityFraud,\n" +
                    "  bankruptcyType,\n" +
                    "  statcCreditRevoked,\n" +
                    "  statcChargeoff,\n" +
                    "  statfReinstateChargeoff,\n" +
                    "  statcCurrentOverlimit,\n" +
                    "  statcWatch,\n" +
                    "  statcTieredAuth,\n" +
                    "  statfCreditCounseling,\n" +
                    "  statfCrv,\n" +
                    "  cardReceiptVerifyInd,\n" +
                    "  dateLastPinChange,\n" +
                    "  dateAccountValidFrom,\n" +
                    "  dateLastCardRequest,\n" +
                    "  reasonLastCardRequest,\n" +
                    "  balanceCurrent,\n" +
                    "  delinquentAmount,\n" +
                    "  statcCardWatch,\n" +
                    "  nameEmboss,\n" +
                    "  customerIdFromHeader,\n" +
                    "  jointCustomerId,\n" +
                    "  numberOfPaymentIds,\n" +
                    "  numberOfAuthorizedUsers,\n" +
                    "  userIndicator01,\n" +
                    "  accountIdentifierNo;";

    public static final String POSTGRES_FILTER_BY_PRIMARY_CUSTOMER_CLAUSE = "  AND ac.ACCOUNT_CUSTOMER_ROLE_UID = 1\n" +
            "  AND ac.ACTIVE_IND = 'Y'";

    public static final String POSTGRES_FILTER_BY_PRODUCT_CLAUSE = "  AND (\n" +
            "    a.PRODUCT_UID IN (7, 8, 9, 10)\n" +
            "    OR (\n" +
            "      SELECT\n" +
            "        COUNT (*)\n" +
            "      FROM\n" +
            "        ACCOUNT a1\n" +
            "        INNER JOIN ACCOUNT_CUSTOMER ac1 ON ac1.CUSTOMER_UID = ac.CUSTOMER_UID\n" +
            "      WHERE\n" +
            "        a1.PRODUCT_UID IN (7, 8, 9, 10)\n" +
            "    ) > 0\n" +
            "  )";

    public static final String POSTGRES_SELECT_FEED_DETAILS_WHERE_ACCOUNT_UID_PRIMARY_KEY_QUERY = POSTGRES_SELECT_FEED_DETAILS_QUERY +
            "\nWHERE a.ACCOUNT_UID = ?" + POSTGRES_FILTER_BY_PRIMARY_CUSTOMER_CLAUSE + POSTGRES_FILTER_BY_PRODUCT_CLAUSE + POSTGRES_GROUP_BY_CLAUSE;

    public static final String POSTGRES_SELECT_FEED_DETAILS_WHERE_ACCESS_MEDIUM_UID_PRIMARY_KEY_QUERY = POSTGRES_SELECT_FEED_DETAILS_QUERY +
            "\nINNER JOIN ACCESS_MEDIUM am ON am.ACCOUNT_CUSTOMER_UID = ac.ACCOUNT_CUSTOMER_UID\n" +
            "WHERE am.ACCESS_MEDIUM_UID = ?" + POSTGRES_FILTER_BY_PRIMARY_CUSTOMER_CLAUSE + POSTGRES_FILTER_BY_PRODUCT_CLAUSE + POSTGRES_GROUP_BY_CLAUSE;

    public static final String POSTGRES_SELECT_FEED_DETAILS_WHERE_CUSTOMER_UID_PRIMARY_KEY_QUERY = POSTGRES_SELECT_FEED_DETAILS_QUERY +
            "\nINNER JOIN CUSTOMER c ON c.CUSTOMER_UID = ac.CUSTOMER_UID\n" +
            "WHERE c.CUSTOMER_UID = ?" + POSTGRES_FILTER_BY_PRIMARY_CUSTOMER_CLAUSE + POSTGRES_FILTER_BY_PRODUCT_CLAUSE + POSTGRES_GROUP_BY_CLAUSE;

    public static final String POSTGRES_SELECT_FEED_DETAILS_WHERE_CUSTOMER_IDENTIFIER_UID_PRIMARY_KEY_QUERY = POSTGRES_SELECT_FEED_DETAILS_QUERY +
            "\nINNER JOIN CUSTOMER c ON c.CUSTOMER_UID = ac.CUSTOMER_UID\n" +
            "INNER JOIN CUSTOMER_IDENTIFIER ci ON ci.CUSTOMER_UID = c.CUSTOMER_UID\n" +
            "WHERE ci.CUSTOMER_IDENTIFIER_UID = ?" + POSTGRES_FILTER_BY_PRIMARY_CUSTOMER_CLAUSE + POSTGRES_FILTER_BY_PRODUCT_CLAUSE + POSTGRES_GROUP_BY_CLAUSE;

    public static final String POSTGRES_SELECT_FEED_DETAILS_WHERE_MAST_ACCOUNT_ID_PRIMARY_KEY_QUERY = POSTGRES_SELECT_FEED_DETAILS_QUERY +
            "WHERE a.ACCOUNT_NO = SUBSTRING(?, 2)" + POSTGRES_FILTER_BY_PRIMARY_CUSTOMER_CLAUSE + POSTGRES_FILTER_BY_PRODUCT_CLAUSE + POSTGRES_GROUP_BY_CLAUSE;
}
