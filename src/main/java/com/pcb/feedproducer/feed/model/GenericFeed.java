package com.pcb.feedproducer.feed.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder(alphabetic = true)
@Data
public class GenericFeed<FEED extends GenericFeed<FEED>> {
    private String tranCode = "102";
    private String workflow = "RBCON";
    private String dataSpecificationVersion = "2.0";
    private String clientIdFromHeader = "PCF";
    private String recordCreationDate = StringUtils.EMPTY;
    private String recordCreationTime = StringUtils.EMPTY;
    private String recordCreationMilliseconds = StringUtils.EMPTY;
    private String gmtOffset = StringUtils.EMPTY;
    private String customerIdFromHeader = StringUtils.EMPTY;
    private String externalTransactionId = StringUtils.EMPTY;
}
