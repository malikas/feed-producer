package com.pcb.feedproducer.feed.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Builder
@Data
public class FeedDetails {
    private final String customerAcctNumber;
    private final Integer productId;
    private final Integer numberOfCyclesInactive;
    private final LocalDateTime openDate;
    private final String limitCredit;
    private final String statcCurrentPastDue;
    private final String customData81;
    private final String dateLastBilling;
    private final String statcClosed;
    private final String statcSecurityFraud;
    private final String bankruptcyType;
    private final String statcCreditRevoked;
    private final String statcChargeoff;
    private final String statfReinstateChargeoff;
    private final String statcCurrentOverlimit;
    private final String statcWatch;
    private final String statcTieredAuth;
    private final String statfCreditCounseling;
    private final String statfCrv;
    private final String cardReceiptVerifyInd;
    private final String dateLastPinChange;
    private final String dateAccountValidFrom;
    private final String dateLastCardRequest;
    private final String reasonLastCardRequest;
    private final String balanceCurrent;
    private final String delinquentAmount;
    private final String statcCardWatch;
    private final String nameEmboss;
    private final String customerIdFromHeader;
    private final String jointCustomerId;
    private final Integer numberOfPaymentIds;
    private final Integer numberOfAuthorizedUsers;
    private final String userIndicator01;
    private final String accountIdentifierNo;
}
