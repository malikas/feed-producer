package com.pcb.feedproducer.feed.generator;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.ImmutableSet;
import com.google.common.eventbus.Subscribe;
import com.pcb.feedproducer.cdcevent.model.CdcAccessMediumEvent;
import com.pcb.feedproducer.cdcevent.model.CdcAccountCustomerEvent;
import com.pcb.feedproducer.cdcevent.model.CdcAccountEvent;
import com.pcb.feedproducer.cdcevent.model.CdcAccountIdentifierEvent;
import com.pcb.feedproducer.cdcevent.model.CdcCustomerEvent;
import com.pcb.feedproducer.cdcevent.model.CdcCustomerIdentifierEvent;
import com.pcb.feedproducer.cdcevent.model.CdcEvent;
import com.pcb.feedproducer.cdcevent.model.CdcEventEnvelope;
import com.pcb.feedproducer.cdcevent.model.CdcInteracAutodepRegisterEvent;
import com.pcb.feedproducer.cdcevent.model.CdcTsysAm00Event;
import com.pcb.feedproducer.cdcevent.model.CdcTsysAm01Event;
import com.pcb.feedproducer.cdcevent.model.CdcTsysAm02Event;
import com.pcb.feedproducer.cdcevent.model.CdcTsysAm04Event;
import com.pcb.feedproducer.cdcevent.model.CdcTsysAm0aEvent;
import com.pcb.feedproducer.cdcevent.model.CdcTsysAm0eEvent;
import com.pcb.feedproducer.feed.generator.exception.FeedGeneratorException;
import com.pcb.feedproducer.feed.model.AisFeed;
import com.pcb.feedproducer.feed.model.FeedDetails;
import com.pcb.feedproducer.util.JacksonMapperFactory;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static com.pcb.feedproducer.feed.FeedResolver.PRODUCT_ID_TO_PRODUCT_TYPE;
import static com.pcb.feedproducer.feed.FeedResolver.Product.ADDITIONAL;
import static com.pcb.feedproducer.feed.FeedResolver.Product.CREDIT_CARD;
import static com.pcb.feedproducer.feed.FeedResolver.Product.GOAL;
import static com.pcb.feedproducer.feed.FeedResolver.Product.INDIVIDUAL;
import static com.pcb.feedproducer.feed.FeedResolver.Product.JOINT;

@Service
@Slf4j
public class AisFeedGenerator extends BaseFeedGenerator<AisFeed> {
    private static final Set<Class<? extends CdcEvent<?>>> ELIGIBLE_CDC_EVENTS = ImmutableSet.of(
            CdcAccountEvent.class,
            CdcCustomerEvent.class,
            CdcAccountCustomerEvent.class,
            CdcAccessMediumEvent.class,
            CdcAccountIdentifierEvent.class,
            CdcCustomerIdentifierEvent.class,
            CdcInteracAutodepRegisterEvent.class,
            CdcTsysAm00Event.class,
            CdcTsysAm01Event.class,
            CdcTsysAm02Event.class,
            CdcTsysAm04Event.class,
            CdcTsysAm0aEvent.class,
            CdcTsysAm0eEvent.class
    );

    @Value("${AisFeedGenerator.feedTopic}")
    private String feedTopic;

    private final Timer generateAisFeedTimer;

    public AisFeedGenerator(MetricRegistry metricRegistry, KafkaTemplate<String, AisFeed> kafkaTemplate) {
        super(metricRegistry, kafkaTemplate);

        this.generateAisFeedTimer = metricRegistry.timer("generateAisFeedTimer");
    }

    @Override
    @Subscribe
    public void generateFeed(ImmutablePair<CdcEventEnvelope<? extends CdcEvent<?>>, List<FeedDetails>> feedDetailsPair) {
        Objects.requireNonNull(feedDetailsPair);

        CdcEventEnvelope<? extends CdcEvent<?>> cdcEventEnvelope = Objects.requireNonNull(feedDetailsPair.getLeft());
        List<FeedDetails> feedDetailsList = Objects.requireNonNull(feedDetailsPair.getRight());
        if (!isCdcEventEligibleForFeed(cdcEventEnvelope)) {
            log.debug("CdcEvent is not eligible for AisFeed: {}", cdcEventEnvelope);
            return;
        }

        for (FeedDetails feedDetails : feedDetailsList) {
            generateAisFeedTimer.time(() -> {
                var aisFeed = new AisFeed();
                populateFeedWithCommonValues(aisFeed);
                populateCustomerIdFromHeader(aisFeed, feedDetails);
                populateCustomerAcctNumber(aisFeed, feedDetails);
                populateType(aisFeed, feedDetails);
                populateJointCustomerId(aisFeed, feedDetails);
                populateRoutingNumber(aisFeed, feedDetails);
                populateApplicationReferenceNumber(aisFeed, feedDetails);
                populateNumberOfPaymentIds(aisFeed, feedDetails);
                populateNumberOfAuthorizedUsers(aisFeed, feedDetails);
                populateOpenDate(aisFeed, feedDetails);
                populateStatus(aisFeed, feedDetails);
                populateStatusDate(aisFeed, feedDetails);
                populateCreditLimit(aisFeed, feedDetails);
                populateOverdraftLimit(aisFeed, feedDetails);
                populateDailyPosLimit(aisFeed, feedDetails);
                populateDailyCashLimit(aisFeed, feedDetails);
                populateDailyTotalLimit(aisFeed, feedDetails);
                populateStatementDayOfMonth(aisFeed, feedDetails);
                populateNumberOfCyclesInactive(aisFeed, feedDetails);
                populateNumberOfCyclesDelinquent(aisFeed, feedDetails);
                populateDelinquentAmount(aisFeed, feedDetails);
                populateOverlimitFlag(aisFeed, feedDetails);
                populateUserIndicator01(aisFeed, feedDetails);

                try {
                    var sendResultListenableFuture = kafkaTemplate.send(feedTopic, aisFeed);
                    sendResultListenableFuture.get(5, TimeUnit.SECONDS);
                    log.debug("Produced AisFeed to Kafka: {}", JacksonMapperFactory.getMapper().writeValueAsString(aisFeed));
                } catch (JsonProcessingException | ExecutionException | InterruptedException | TimeoutException e) {
                    log.error("Error occurred while producing AisFeed to Kafka topic: {}->{}", aisFeed, feedTopic, e);
                    throw new FeedGeneratorException(e);
                }
            });
        }
    }

    @Override
    protected boolean isCdcEventEligibleForFeed(CdcEventEnvelope<? extends CdcEvent<?>> cdcEventEnvelope) {
        Objects.requireNonNull(cdcEventEnvelope);

        if (cdcEventEnvelope.getOpType() == CdcEventEnvelope.OperationType.I || cdcEventEnvelope.getBeforeEvent() == null) {
            return ELIGIBLE_CDC_EVENTS.contains(cdcEventEnvelope.getAfterEvent().getClass());
        }

        if (cdcEventEnvelope.getAfterEvent() instanceof CdcAccountEvent) {
            CdcAccountEvent beforeEvent = (CdcAccountEvent) cdcEventEnvelope.getBeforeEvent();
            CdcAccountEvent afterEvent = (CdcAccountEvent) cdcEventEnvelope.getAfterEvent();
            return !afterEvent.getInactiveCycles().equals(beforeEvent.getInactiveCycles()) ||
                    !afterEvent.getOpenDt().equals(beforeEvent.getOpenDt());
        } else if (cdcEventEnvelope.getAfterEvent() instanceof CdcCustomerEvent) {
            return false;
        } else if (cdcEventEnvelope.getAfterEvent() instanceof CdcAccountCustomerEvent) {
            CdcAccountCustomerEvent beforeEvent = (CdcAccountCustomerEvent) cdcEventEnvelope.getBeforeEvent();
            CdcAccountCustomerEvent afterEvent = (CdcAccountCustomerEvent) cdcEventEnvelope.getAfterEvent();
            return !afterEvent.getAccountCustomerRoleUid().equals(beforeEvent.getAccountCustomerRoleUid()) ||
                    !afterEvent.getActiveInd().equals(beforeEvent.getActiveInd());
        } else if (cdcEventEnvelope.getAfterEvent() instanceof CdcAccessMediumEvent) {
            CdcAccessMediumEvent beforeEvent = (CdcAccessMediumEvent) cdcEventEnvelope.getBeforeEvent();
            CdcAccessMediumEvent afterEvent = (CdcAccessMediumEvent) cdcEventEnvelope.getAfterEvent();
            return !afterEvent.getDeactivatedDt().equals(beforeEvent.getDeactivatedDt()) ||
                    !afterEvent.getExpiryDt().equals(beforeEvent.getExpiryDt());
        } else if (cdcEventEnvelope.getAfterEvent() instanceof CdcAccountIdentifierEvent) {
            CdcAccountIdentifierEvent beforeEvent = (CdcAccountIdentifierEvent) cdcEventEnvelope.getBeforeEvent();
            CdcAccountIdentifierEvent afterEvent = (CdcAccountIdentifierEvent) cdcEventEnvelope.getAfterEvent();
            return !afterEvent.getAccountIdentifierNo().equals(beforeEvent.getAccountIdentifierNo()) ||
                    !afterEvent.getType().equals(beforeEvent.getType());
        } else if (cdcEventEnvelope.getAfterEvent() instanceof CdcCustomerIdentifierEvent) {
            CdcCustomerIdentifierEvent beforeEvent = (CdcCustomerIdentifierEvent) cdcEventEnvelope.getBeforeEvent();
            CdcCustomerIdentifierEvent afterEvent = (CdcCustomerIdentifierEvent) cdcEventEnvelope.getAfterEvent();
            return !afterEvent.getCustomerIdentifierNo().equals(beforeEvent.getCustomerIdentifierNo()) ||
                    !afterEvent.getType().equals(beforeEvent.getType()) ||
                    !afterEvent.getDisabledInd().equals(beforeEvent.getDisabledInd());
        } else if (cdcEventEnvelope.getAfterEvent() instanceof CdcInteracAutodepRegisterEvent) {
            CdcInteracAutodepRegisterEvent beforeEvent = (CdcInteracAutodepRegisterEvent) cdcEventEnvelope.getBeforeEvent();
            CdcInteracAutodepRegisterEvent afterEvent = (CdcInteracAutodepRegisterEvent) cdcEventEnvelope.getAfterEvent();
            return !afterEvent.getActiveInd().equals(beforeEvent.getActiveInd());
        } else {
            return false;
        }
    }

    @Override
    public void populateExternalTransactionId(AisFeed feed) {
        feed.setExternalTransactionId("AISA" + externalTransactionIdGenerator.generate(28));
    }

    private void populateCustomerIdFromHeader(AisFeed feed, FeedDetails feedDetails) {
        feed.setCustomerIdFromHeader(feedDetails.getCustomerIdFromHeader());
    }

    private void populateCustomerAcctNumber(AisFeed feed, FeedDetails feedDetails) {
        feed.setCustomerAcctNumber(feedDetails.getCustomerAcctNumber());
    }

    private void populateType(AisFeed feed, FeedDetails feedDetails) {
        var type = StringUtils.EMPTY;
        var product = PRODUCT_ID_TO_PRODUCT_TYPE.get(feedDetails.getProductId());
        if (product.equals(CREDIT_CARD)) {
            type = "C";
        } else if (product.equals(INDIVIDUAL) || product.equals(JOINT)) {
            type = "D";
        } else if (product.equals(GOAL)) {
            type = "S";
        }
        feed.setType(type);
    }

    private void populateJointCustomerId(AisFeed feed, FeedDetails feedDetails) {
        feed.setJointCustomerId(feedDetails.getJointCustomerId());
    }

    private void populateRoutingNumber(AisFeed feed, FeedDetails feedDetails) {
        if (StringUtils.isBlank(feedDetails.getAccountIdentifierNo())) {
            return;
        }
        var product = PRODUCT_ID_TO_PRODUCT_TYPE.get(feedDetails.getProductId());
        if (product.equals(INDIVIDUAL) || product.equals(JOINT) || product.equals(ADDITIONAL)) {
            feed.setRoutingNumber("320-02002-" + feedDetails.getAccountIdentifierNo());
        }
    }

    private void populateApplicationReferenceNumber(AisFeed feed, FeedDetails feedDetails) {
        feed.setApplicationReferenceNumber(feedDetails.getCustomData81());
    }

    private void populateNumberOfPaymentIds(AisFeed feed, FeedDetails feedDetails) {
        feed.setNumberOfPaymentIds(feedDetails.getNumberOfPaymentIds().toString());
    }

    private void populateNumberOfAuthorizedUsers(AisFeed feed, FeedDetails feedDetails) {
        feed.setNumberOfAuthorizedUsers(feedDetails.getNumberOfAuthorizedUsers().toString());
    }

    private void populateOpenDate(AisFeed feed, FeedDetails feedDetails) {
        feed.setOpenDate(feedDetails.getOpenDate().format(YYYY_MM_DD));
    }

    private void populateStatus(AisFeed feed, FeedDetails feedDetails) {
        // Filter for AM00_STATC_CLOSED field
        String statcClosed = feedDetails.getStatcClosed();
        if (StringUtils.isNotBlank(statcClosed)) {
            if (statcClosed.equals("PE") || statcClosed.equals("BP")) {
                feed.setStatus("20");
                return;
            }
        }

        // Filter for AM00_STATC_SECURITY_FRAUD field
        String statcSecurityFraud = feedDetails.getStatcSecurityFraud();
        if (StringUtils.isNotBlank(statcSecurityFraud)) {
            if (statcSecurityFraud.equals("FA") || statcSecurityFraud.equals("IA")) {
                feed.setStatus("20");
                return;
            }

            if (statcSecurityFraud.equals("AT") || statcSecurityFraud.equals("4L")) {
                feed.setStatus("30");
                return;
            }

            feed.setStatus("25");
            return;
        }

        // Filter for AM00_BANKRUPTCY_TYPE field
        String bankruptcyType = feedDetails.getBankruptcyType();
        if (StringUtils.isNotBlank(bankruptcyType) && !bankruptcyType.equals("0")) {
            feed.setStatus("22");
            return;
        }

        // Filter for AM00_STATC_CREDIT_REVOKED field
        String statcCreditRevoked = feedDetails.getStatcCreditRevoked();
        if (StringUtils.isNotBlank(statcCreditRevoked)) {
            feed.setStatus("23");
            return;
        }

        // Filter for AM00_STATC_CHARGEOFF field
        String statcChargeoff = feedDetails.getStatcChargeoff();
        if (StringUtils.isNotBlank(statcChargeoff)) {
            feed.setStatus("24");
            return;
        }

        // Filter for AM00_STATC_CLOSED field
        if (StringUtils.isNotBlank(statcClosed)) {
            if (statcClosed.equals("DC")) {
                feed.setStatus("28");
                return;
            }

            if (statcClosed.equals("DU")) {
                feed.setStatus("29");
                return;
            }

            if (statcClosed.charAt(0) == 'V') {
                feed.setStatus("21");
                return;
            }

            feed.setStatus("31");
            return;
        }

        // Filter for AM00_STATF_REINSTATE_CHARGEOFF field
        String statfReinstateChargeoff = feedDetails.getStatfReinstateChargeoff();
        if (StringUtils.isNotBlank(statfReinstateChargeoff)) {
            feed.setStatus("02");
            return;
        }

        // TODO:
        // Filter for AM00_STATC_CLOSED - whenever the old object's value is not null and the new object's value is null
//        if (beforeOpt.isPresent()) {
//            Optional<String> statcClosedBeforeOptional = beforeOpt.get().getValueColumn(prefixedColumnName(AM00_STATC_CLOSED));
//            if (!isColumnValueNull(statcClosedBeforeOptional) && isColumnValueNull(statcClosedAfterOptional)) {
//                return after.withColumn(outputColumn, "02");
//            }
//        }

        // Filter for AM00_STATC_CURRENT_OVERLIMIT field
        String statcCurrentOverlimit = feedDetails.getStatcCurrentOverlimit();
        if (StringUtils.isNotBlank(statcCurrentOverlimit)) {
            feed.setStatus("03");
            return;
        }

        // Filter for AM00_STATC_WATCH field
        String statcWatch = feedDetails.getStatcWatch();
        if (StringUtils.isNotBlank(statcWatch)) {
            if (statcWatch.equals("SF") || statcWatch.equals("OT")) {
                feed.setStatus("05");
                return;
            }
        }

        // Filter for AM0A_STATC_CARD_WATCH field
        String statcCardWatch = feedDetails.getStatcCardWatch();
        if (StringUtils.isNotBlank(statcCardWatch)) {
            if (statcCardWatch.equals("01") || statcCardWatch.equals("02") || statcCardWatch.equals("04") || statcCardWatch.equals("06") || statcCardWatch.equals("15")) {
                feed.setStatus("05");
                return;
            }
        }

        // Filter for AM00_STATC_TIERED_AUTH
        String statcTieredAuth = feedDetails.getStatcTieredAuth();
        if (StringUtils.isNotBlank(statcTieredAuth)) {
            if (statcTieredAuth.equals("10") || statcTieredAuth.equals("18") || statcTieredAuth.equals("19") || statcTieredAuth.equals("20") || statcTieredAuth.equals("21")) {
                feed.setStatus("05");
                return;
            }

            if (statcTieredAuth.equals("12") || statcTieredAuth.equals("17")) {
                feed.setStatus("04");
                return;
            }
        }

        // Filter for AM0A_STATC_CARD_WATCH
        if (StringUtils.isNotBlank(statcCardWatch)) {
            if (statcCardWatch.equals("13")) {
                feed.setStatus("04");
                return;
            }
        }

        // Filter for AM00_STATC_TIERED_AUTH
        if (StringUtils.isNotBlank(statcTieredAuth)) {
            if (statcTieredAuth.equals("33") || statcTieredAuth.equals("34") || statcTieredAuth.equals("35")) {
                feed.setStatus("06");
                return;
            }
        }

        // Filter for AM00_STATF_CREDIT_COUNSELING field
        String statfCreditCounseling = feedDetails.getStatfCreditCounseling();
        if (StringUtils.isNotBlank(statfCreditCounseling)) {
            if (statfCreditCounseling.equals("Y")) {
                feed.setStatus("07");
                return;
            }
        }

        // Filter for AM00_STATC_CURRENT_PAST_DUE field
        String statcCurrentPastDue = feedDetails.getStatcCurrentPastDue();
        if (StringUtils.isNotBlank(statcCurrentPastDue)) {
            feed.setStatus("08");
            return;
        }

        // Filter for AM0A_STATC_CARD_WATCH field
        if (StringUtils.isNotBlank(statcCardWatch)) {
            if (statcCardWatch.equals("20") || statcCardWatch.equals("21") || statcCardWatch.equals("22") || statcCardWatch.equals("23")) {
                feed.setStatus("00");
                return;
            }
        }

        // Filter for AM00_STATF_CRV field
        String statfCrv = feedDetails.getStatfCrv();
        if (StringUtils.isNotBlank(statfCrv)) {
            if (statfCrv.equals("Y")) {
                feed.setStatus("11");
                return;
            }

            feed.setStatus("01");
            return;
        }
    }

    private void populateStatusDate(AisFeed feed, FeedDetails feedDetails) {
        feed.setStatusDate(LocalDate.now(UTC_ZONE).format(YYYY_MM_DD));
    }

    private void populateCreditLimit(AisFeed feed, FeedDetails feedDetails) {
        var product = PRODUCT_ID_TO_PRODUCT_TYPE.get(feedDetails.getProductId());
        if (product.equals(CREDIT_CARD)) {
            feed.setCreditLimit(feedDetails.getLimitCredit());
        }
    }

    private void populateOverdraftLimit(AisFeed feed, FeedDetails feedDetails) {
        var product = PRODUCT_ID_TO_PRODUCT_TYPE.get(feedDetails.getProductId());
        if (product.equals(INDIVIDUAL) || product.equals(JOINT)) {
            feed.setOverdraftLimit(feedDetails.getLimitCredit());
        }
    }

    private void populateDailyPosLimit(AisFeed feed, FeedDetails feedDetails) {
        var product = PRODUCT_ID_TO_PRODUCT_TYPE.get(feedDetails.getProductId());
        if (!product.equals(GOAL)) {
            feed.setDailyPosLimit("50000");
        }
    }

    private void populateDailyCashLimit(AisFeed feed, FeedDetails feedDetails) {
        var product = PRODUCT_ID_TO_PRODUCT_TYPE.get(feedDetails.getProductId());
        if (!product.equals(GOAL)) {
            feed.setDailyCashLimit("5000");
        }
    }

    private void populateDailyTotalLimit(AisFeed feed, FeedDetails feedDetails) {
        var product = PRODUCT_ID_TO_PRODUCT_TYPE.get(feedDetails.getProductId());
        if (!product.equals(GOAL)) {
            feed.setDailyTotalLimit("50000");
        }
    }

    private void populateStatementDayOfMonth(AisFeed feed, FeedDetails feedDetails) {
        feed.setStatementDayOfMonth(LocalDate.parse(feedDetails.getDateLastBilling(), YYY_DDD).format(DAY));
    }

    private void populateNumberOfCyclesInactive(AisFeed feed, FeedDetails feedDetails) {
        feed.setNumberOfCyclesInactive(feedDetails.getNumberOfCyclesInactive().toString());
    }

    private void populateNumberOfCyclesDelinquent(AisFeed feed, FeedDetails feedDetails) {
        var numberOfCyclesDelinquent = feedDetails.getStatcCurrentPastDue();
        switch (numberOfCyclesDelinquent) {
            case "1A":
                numberOfCyclesDelinquent = "10";
                break;
            case "1B":
                numberOfCyclesDelinquent = "11";
                break;
            case "1C":
                numberOfCyclesDelinquent = "12";
                break;
        }
        feed.setNumberOfCyclesDelinquent(numberOfCyclesDelinquent);
    }

    private void populateDelinquentAmount(AisFeed feed, FeedDetails feedDetails) {
        try {
            feed.setDelinquentAmount(String.format("%.2f", Double.valueOf(feedDetails.getDelinquentAmount())));
        } catch (NumberFormatException e) {
            log.error("Error occurred while populating delinquentAmount", e);
        }
    }

    private void populateOverlimitFlag(AisFeed feed, FeedDetails feedDetails) {
        if (StringUtils.isNotBlank(feedDetails.getBalanceCurrent()) && StringUtils.isNotBlank(feedDetails.getLimitCredit())) {
            try {
                Double balanceCurrent = Double.valueOf(feedDetails.getBalanceCurrent());
                Double limitCredit = Double.valueOf(feedDetails.getLimitCredit());
                feed.setOverlimitFlag(balanceCurrent.compareTo(limitCredit) > 0 ? "1" : "0");
            } catch (NumberFormatException e) {
                log.error("Error occurred while populating overlimitFlag", e);
            }
        }
    }

    private void populateUserIndicator01(AisFeed feed, FeedDetails feedDetails) {
        feed.setUserIndicator01(StringUtils.isNotBlank(feedDetails.getUserIndicator01()) ? feedDetails.getUserIndicator01() : "N");
    }
}
