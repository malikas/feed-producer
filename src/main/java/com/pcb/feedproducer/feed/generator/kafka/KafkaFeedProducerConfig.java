package com.pcb.feedproducer.feed.generator.kafka;

import com.pcb.feedproducer.feed.model.GenericFeed;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.Objects;

@Configuration
@EnableConfigurationProperties(KafkaProperties.class)
@Slf4j
public class KafkaFeedProducerConfig {
    private final KafkaProperties kafkaProperties;

    public KafkaFeedProducerConfig(KafkaProperties kafkaProperties) {
        this.kafkaProperties = Objects.requireNonNull(kafkaProperties);
    }

    @Bean(name = "feedProducerKafkaTemplate")
    public <FEED extends GenericFeed<FEED>> KafkaTemplate<String, FEED> feedProducerKafkaTemplate() {
        return new KafkaTemplate<>(new DefaultKafkaProducerFactory<>(kafkaProperties.buildProducerProperties()));
    }
}
