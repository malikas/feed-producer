package com.pcb.feedproducer.feed.generator.exception;

public class FeedGeneratorException extends RuntimeException {
    public FeedGeneratorException(String message) {
        super(message);
    }

    public FeedGeneratorException(String message, Throwable cause) {
        super(message, cause);
    }

    public FeedGeneratorException(Throwable cause) {
        super(cause);
    }
}
