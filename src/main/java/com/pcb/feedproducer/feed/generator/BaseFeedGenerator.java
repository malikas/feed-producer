package com.pcb.feedproducer.feed.generator;

import com.codahale.metrics.MetricRegistry;
import com.pcb.feedproducer.cdcevent.model.CdcEvent;
import com.pcb.feedproducer.cdcevent.model.CdcEventEnvelope;
import com.pcb.feedproducer.feed.model.FeedDetails;
import com.pcb.feedproducer.feed.model.GenericFeed;
import com.pcb.feedproducer.util.LocalEventBus;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.text.CharacterPredicates;
import org.apache.commons.text.RandomStringGenerator;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;

@Service
public abstract class BaseFeedGenerator<FEED extends GenericFeed<FEED>> implements FeedGenerator<FEED> {
    protected static final DateTimeFormatter YYYY_MM_DD = DateTimeFormatter.ofPattern("yyyyMMdd");
    protected static final DateTimeFormatter YYY_DDD = DateTimeFormatter.ofPattern("yyyDDD");
    protected static final DateTimeFormatter HH_MM_SS = DateTimeFormatter.ofPattern("HHmmss");
    protected static final DateTimeFormatter SSS = DateTimeFormatter.ofPattern("SSS");
    protected static final DateTimeFormatter OFFSET = DateTimeFormatter.ofPattern("ZZZZZ");
    protected static final DateTimeFormatter DAY = DateTimeFormatter.ofPattern("d");

    protected static final ZoneId UTC_ZONE = ZoneOffset.UTC;
    protected static final ZoneId ET = ZoneId.of("America/New_York");

    protected final RandomStringGenerator externalTransactionIdGenerator = new RandomStringGenerator.Builder()
            .withinRange('0', 'z')
            .filteredBy(CharacterPredicates.DIGITS, CharacterPredicates.LETTERS)
            .build();

    protected final KafkaTemplate<String, FEED> kafkaTemplate;

    public BaseFeedGenerator(MetricRegistry metricRegistry, @Qualifier("feedProducerKafkaTemplate") KafkaTemplate<String, FEED> kafkaTemplate) {
        this.kafkaTemplate = Objects.requireNonNull(kafkaTemplate);
        LocalEventBus.register(this);
    }

    public abstract void generateFeed(ImmutablePair<CdcEventEnvelope<? extends CdcEvent<?>>, List<FeedDetails>> feedDetailsPair);

    protected abstract boolean isCdcEventEligibleForFeed(CdcEventEnvelope<? extends CdcEvent<?>> cdcEventEnvelope);

    @Override
    public void populateFeedWithCommonValues(FEED feed) {
        populateRecordCreationDate(feed);
        populateRecordCreationTime(feed);
        populateRecordCreationMilliseconds(feed);
        populateGmtOffset(feed);
        populateExternalTransactionId(feed);
    }

    @Override
    public void populateRecordCreationDate(FEED feed) {
        feed.setRecordCreationDate(LocalDate.now(UTC_ZONE).format(YYYY_MM_DD));
    }

    @Override
    public void populateRecordCreationTime(FEED feed) {
        feed.setRecordCreationTime(LocalDateTime.now(UTC_ZONE).format(HH_MM_SS));
    }

    @Override
    public void populateRecordCreationMilliseconds(FEED feed) {
        feed.setRecordCreationMilliseconds(LocalDateTime.now(UTC_ZONE).format(SSS));
    }

    @Override
    public void populateGmtOffset(FEED feed) {
        feed.setGmtOffset(LocalDateTime.now().atZone(ET).format(OFFSET));
    }

    @PreDestroy
    public void shutdown() {
        LocalEventBus.unregister(this);
    }
}
