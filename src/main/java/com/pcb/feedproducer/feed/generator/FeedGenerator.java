package com.pcb.feedproducer.feed.generator;

import com.pcb.feedproducer.feed.model.GenericFeed;

public interface FeedGenerator<FEED extends GenericFeed<FEED>> {
    void populateFeedWithCommonValues(FEED feed);

    void populateRecordCreationDate(FEED feed);

    void populateRecordCreationTime(FEED feed);

    void populateRecordCreationMilliseconds(FEED feed);

    void populateGmtOffset(FEED feed);

    void populateExternalTransactionId(FEED feed);
}
