package com.pcb.feedproducer.feed;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.pcb.feedproducer.cdcevent.model.CdcEvent;
import com.pcb.feedproducer.cdcevent.model.CdcEventEnvelope;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.Objects;
import java.util.Set;

@Slf4j
public class FeedResolver {
    @Getter
    public enum Product {
        CREDIT_CARD("CREDIT-CARD", ImmutableSet.of(1, 2, 3, 4, 5, 6)),
        INDIVIDUAL("INDIVIDUAL", ImmutableSet.of(7)),
        GOAL("GOAL", ImmutableSet.of(8)),
        JOINT("JOINT", ImmutableSet.of(9)),
        ADDITIONAL("ADDITIONAL", ImmutableSet.of(10));

        private final String productType;
        private final Set<Integer> productIds;

        Product(String productType, Set<Integer> productIds) {
            this.productType = productType;
            this.productIds = productIds;
        }
    }

    public static final Map<Integer, Product> PRODUCT_ID_TO_PRODUCT_TYPE = Map.ofEntries(
            Map.entry(1, Product.CREDIT_CARD),
            Map.entry(2, Product.CREDIT_CARD),
            Map.entry(3, Product.CREDIT_CARD),
            Map.entry(4, Product.CREDIT_CARD),
            Map.entry(5, Product.CREDIT_CARD),
            Map.entry(6, Product.CREDIT_CARD),
            Map.entry(7, Product.INDIVIDUAL),
            Map.entry(8, Product.GOAL),
            Map.entry(9, Product.JOINT),
            Map.entry(10, Product.ADDITIONAL)
    );
    public static final Set<Integer> PCMC_PRODUCT_IDS = Product.CREDIT_CARD.getProductIds();
    public static final Set<Integer> PCMA_PRODUCT_IDS = ImmutableSet.copyOf(Iterables.concat(Product.INDIVIDUAL.getProductIds(), Product.GOAL.getProductIds(), Product.JOINT.getProductIds(), Product.ADDITIONAL.getProductIds()));
    public static final Set<String> PCMC_PRODUCT_TYPES = ImmutableSet.of(Product.CREDIT_CARD.getProductType());
    public static final Set<String> PCMA_PRODUCT_TYPES = ImmutableSet.of(Product.INDIVIDUAL.getProductType(), Product.GOAL.getProductType(), Product.JOINT.getProductType(), Product.ADDITIONAL.getProductType());

    public static boolean isCdcEventEligibleForFeed(CdcEventEnvelope<? extends CdcEvent<?>> cdcEventEnvelope) {
        Objects.requireNonNull(cdcEventEnvelope);

        return cdcEventEnvelope.getOpType() == CdcEventEnvelope.OperationType.I
                || cdcEventEnvelope.getBeforeEvent() == null
                || wasCdcEventChanged(cdcEventEnvelope.getBeforeEvent(), cdcEventEnvelope.getAfterEvent());
    }

    public static boolean isPcmcFeed(Integer productId) {
        Objects.requireNonNull(productId);
        return PCMC_PRODUCT_IDS.contains(productId);
    }

    public static boolean isPcmcFeed(String productType) {
        Objects.requireNonNull(productType);
        return PCMC_PRODUCT_TYPES.contains(productType);
    }

    public static boolean isPcmaFeed(Integer productId) {
        Objects.requireNonNull(productId);
        return PCMA_PRODUCT_IDS.contains(productId);
    }

    public static boolean isPcmaFeed(String productType) {
        Objects.requireNonNull(productType);
        return PCMA_PRODUCT_TYPES.contains(productType);
    }

    private static boolean wasCdcEventChanged(CdcEvent<?> beforeEvent, CdcEvent<?> afterEvent) {
        if (beforeEvent == null) {
            return true;
        } else {
            return !beforeEvent.equals(afterEvent);
        }
    }
}
