package com.pcb.feedproducer.feed;

import com.pcb.feedproducer.cdcevent.model.CdcEvent;
import com.pcb.feedproducer.cdcevent.model.CdcEventEnvelope;
import com.pcb.feedproducer.cdcevent.persistence.CdcEventPersistence;
import com.pcb.feedproducer.util.LocalEventBus;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@Slf4j
public class FeedProcessor {
    private final CdcEventPersistence cdcEventPersistence;

    public FeedProcessor(CdcEventPersistence cdcEventPersistence) {
        this.cdcEventPersistence = Objects.requireNonNull(cdcEventPersistence);
    }

    public void generateFeedsForCdcEvent(CdcEventEnvelope<? extends CdcEvent<?>> cdcEventEnvelope) {
        Objects.requireNonNull(cdcEventEnvelope);
        if (FeedResolver.isCdcEventEligibleForFeed(cdcEventEnvelope)) {
            var feedDetailsList = cdcEventPersistence.fetchFeedDetails(cdcEventEnvelope);
            if (feedDetailsList.isEmpty()) {
                log.debug("FeedDetails are empty!");
                return;
            }

            LocalEventBus.postAsync(new ImmutablePair<>(cdcEventEnvelope, feedDetailsList));
        } else {
            log.debug("CdcEvent is not eligible for feeds: {}", cdcEventEnvelope);
        }
    }
}
