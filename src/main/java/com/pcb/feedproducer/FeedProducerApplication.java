package com.pcb.feedproducer;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.health.HealthCheckRegistry;
import com.codahale.metrics.jvm.CachedThreadStatesGaugeSet;
import com.codahale.metrics.jvm.GarbageCollectorMetricSet;
import com.codahale.metrics.jvm.MemoryUsageGaugeSet;
import com.codahale.metrics.servlets.AdminServlet;
import com.codahale.metrics.servlets.HealthCheckServlet;
import com.codahale.metrics.servlets.MetricsServlet;
import com.pcb.feedproducer.util.LocalEventBus;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PreDestroy;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import java.util.concurrent.TimeUnit;

/*
TODO:
- DONE: Add support for TSYS-based events to CdcEventEnvelope
- DONE: Add remaining CdcEvents
- DONE(Except for a small piece in Status impl): Complete AisFeed
- DONE: Add metrics
- DONE: Check all exceptions
- DONE: Finish the logic in FeedResolver
- Check whether transactional processing is needed
- NonMon codes
- Add Feed Filters
- Build the queries for other feeds, i.e., CIS and PIS
 */
@SpringBootApplication
@Configuration
@Slf4j
public class FeedProducerApplication {
    public static void main(String[] args) {
        log.info("_STARTING_");
        SpringApplication.run(FeedProducerApplication.class, args);
        log.info("_STARTED_");
    }

    @Bean
    public MetricRegistry metricRegistry() {
        MetricRegistry metricRegistry = new MetricRegistry();
        metricRegistry.register("gc", new GarbageCollectorMetricSet());
        metricRegistry.register("threads", new CachedThreadStatesGaugeSet(10, TimeUnit.SECONDS));
        metricRegistry.register("memory", new MemoryUsageGaugeSet());
        return metricRegistry;
    }

    @Bean
    public HealthCheckRegistry healthCheckRegistry() {
        return new HealthCheckRegistry();
    }

    @Bean
    public ServletRegistrationBean<Servlet> servletRegistrationBean(ServletContext servletContext,
                                                                    MetricRegistry metricRegistry,
                                                                    HealthCheckRegistry healthCheckRegistry) {
        servletContext.setAttribute(MetricsServlet.METRICS_REGISTRY, metricRegistry);
        servletContext.setAttribute(HealthCheckServlet.HEALTH_CHECK_REGISTRY, healthCheckRegistry);
        return new ServletRegistrationBean<>(new AdminServlet(), "/metrics/*");
    }

    @PreDestroy
    public void preDestroy() {
        log.info("Shutting down LocalEventBus");
        LocalEventBus.shutdown();
    }
}
