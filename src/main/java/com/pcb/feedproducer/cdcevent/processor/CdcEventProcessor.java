package com.pcb.feedproducer.cdcevent.processor;

import com.pcb.feedproducer.cdcevent.model.CdcEvent;
import com.pcb.feedproducer.cdcevent.model.CdcEventEnvelope;
import com.pcb.feedproducer.cdcevent.persistence.CdcEventPersistence;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@Slf4j
public class CdcEventProcessor {
    private final CdcEventPersistence cdcEventPersistence;

    public CdcEventProcessor(CdcEventPersistence cdcEventPersistence) {
        this.cdcEventPersistence = Objects.requireNonNull(cdcEventPersistence);
    }

    public void processCdcEvent(CdcEventEnvelope<? extends CdcEvent<?>> cdcEventEnvelope) {
        Objects.requireNonNull(cdcEventEnvelope);
        cdcEventPersistence.persistCdcEvent(cdcEventEnvelope);
    }
}
