package com.pcb.feedproducer.cdcevent.model;

public interface CdcEvent<C extends CdcEvent<C>> {
    Object getPrimaryKey();
}
