package com.pcb.feedproducer.cdcevent.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDateTime;

@Jacksonized
@Builder
@Data
public class CdcTsysAm02Event implements CdcEvent<CdcTsysAm02Event> {
    @JsonProperty("MAST_ACCOUNT_ID") private final String mastAccountId;
    @JsonProperty("AM02_BALANCE_CURRENT") private final String am02BalanceCurrent;
    @JsonProperty("UPDATE_TS") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") private final LocalDateTime updateTs = LocalDateTime.now();

    @Override
    public Object getPrimaryKey() {
        return mastAccountId;
    }
}
