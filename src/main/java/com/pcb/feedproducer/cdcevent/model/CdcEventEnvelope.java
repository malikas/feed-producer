package com.pcb.feedproducer.cdcevent.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.pcb.feedproducer.cdcevent.model.serde.CdcMessageEnvelopeDeserializer;
import com.pcb.feedproducer.util.EnumHelper;

import java.time.LocalDateTime;
import java.util.Objects;

@JsonDeserialize(using = CdcMessageEnvelopeDeserializer.class)
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public class CdcEventEnvelope<CDC_EVENT> {
    public enum OperationType {
        I,
        U,
        UNKNOWN;

        @JsonCreator
        public static OperationType fromString(String strValue) {
            return EnumHelper.fromString(OperationType.class, strValue, UNKNOWN);
        }
    }

    private final String table;
    private final OperationType opType;
    private final LocalDateTime opTs;
    private final LocalDateTime currentTs;
    private final String pos;
    private final CDC_EVENT beforeEvent;
    private final CDC_EVENT afterEvent;

    private CdcEventEnvelope(Builder<CDC_EVENT> builder) {
        this.table = Objects.requireNonNull(builder.table, "table cannot be null");
        this.opType = Objects.requireNonNull(builder.opType, "opType cannot be null");
        this.opTs = Objects.requireNonNull(builder.opTs, "opTs cannot be null");
        this.currentTs = Objects.requireNonNull(builder.currentTs, "currentTs cannot be null");
        this.pos = Objects.requireNonNull(builder.pos, "pos cannot be null");
        this.beforeEvent = builder.beforeEvent; // 'beforeEvent' can be null when the operation is INSERT
        this.afterEvent = Objects.requireNonNull(builder.afterEvent, "afterEvent cannot be null");
    }

    public String getTable() {
        return table;
    }

    public OperationType getOpType() {
        return opType;
    }

    public LocalDateTime getOpTs() {
        return opTs;
    }

    public LocalDateTime getCurrentTs() {
        return currentTs;
    }

    public String getPos() {
        return pos;
    }

    public CDC_EVENT getBeforeEvent() {
        return beforeEvent;
    }

    public CDC_EVENT getAfterEvent() {
        return afterEvent;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CdcEventEnvelope{");
        sb.append("table='").append(table).append('\'');
        sb.append(", opType=").append(opType);
        sb.append(", opTs=").append(opTs);
        sb.append(", currentTs=").append(currentTs);
        sb.append(", pos='").append(pos).append('\'');
        sb.append(", beforeEvent=").append(beforeEvent);
        sb.append(", afterEvent=").append(afterEvent);
        sb.append('}');
        return sb.toString();
    }

    public static <CDC_EVENT> Builder<CDC_EVENT> builder() {
        return new Builder<>();
    }

    @JsonPOJOBuilder(withPrefix = "")
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
    public static final class Builder<CDC_EVENT> {
        private String table;
        private OperationType opType;
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSSSS") private LocalDateTime opTs;
        @JsonFormat(pattern = "yyyy-MM-ddTHH:mm:ss.SSSSSS") private LocalDateTime currentTs;
        private String pos;
        @JsonProperty("before") private CDC_EVENT beforeEvent;
        @JsonProperty("after") private CDC_EVENT afterEvent;

        private Builder() {
        }

        public Builder<CDC_EVENT> table(String table) {
            this.table = table;
            return this;
        }

        public Builder<CDC_EVENT> opType(OperationType opType) {
            this.opType = opType;
            return this;
        }

        public Builder<CDC_EVENT> opTs(LocalDateTime opTs) {
            this.opTs = opTs;
            return this;
        }

        public Builder<CDC_EVENT> currentTs(LocalDateTime currentTs) {
            this.currentTs = currentTs;
            return this;
        }

        public Builder<CDC_EVENT> pos(String pos) {
            this.pos = pos;
            return this;
        }

        public Builder<CDC_EVENT> beforeEvent(CDC_EVENT beforeEvent) {
            this.beforeEvent = beforeEvent;
            return this;
        }

        public Builder<CDC_EVENT> afterEvent(CDC_EVENT afterEvent) {
            this.afterEvent = afterEvent;
            return this;
        }

        public CdcEventEnvelope<CDC_EVENT> build() {
            return new CdcEventEnvelope<>(this);
        }
    }
}
