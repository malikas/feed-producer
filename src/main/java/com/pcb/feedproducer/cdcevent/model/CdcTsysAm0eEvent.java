package com.pcb.feedproducer.cdcevent.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDateTime;

@Jacksonized
@Builder
@Data
public class CdcTsysAm0eEvent implements CdcEvent<CdcTsysAm0eEvent> {
    @JsonProperty("MAST_ACCOUNT_ID") private final String mastAccountId;
    @JsonProperty("AM0E_NAME_EMBOSS") private final String am0eNameEmboss;
    @JsonProperty("UPDATE_TS") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") private final LocalDateTime updateTs = LocalDateTime.now();

    @Override
    public Object getPrimaryKey() {
        return mastAccountId;
    }
}
