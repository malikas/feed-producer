package com.pcb.feedproducer.cdcevent.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDateTime;

@Jacksonized
@Builder
@Data
public class CdcTsysAm01Event implements CdcEvent<CdcTsysAm01Event> {
    @JsonProperty("MAST_ACCOUNT_ID") private final String mastAccountId;
    @JsonProperty("AM01_CUSTOMER_ID") private final String am01CustomerId;
    @JsonProperty("AM01_ACCOUNT_NUM") private final String am01AccountNum;
    @JsonProperty("AM01_CARD_RECEIPT_VERIFY_IND") private final String am01CardReceiptVerifyInd;
    @JsonProperty("AM01_DATE_LAST_PIN_CHANGE") private final String am01DateLastPinChange;
    @JsonProperty("AM01_DATE_ACCOUNT_VALID_FROM") private final String am01DateAccountValidFrom;
    @JsonProperty("AM01_DATE_LAST_CARD_REQUEST") private final String am01DateLastCardRequest;
    @JsonProperty("AM01_REASON_LAST_CARD_REQUEST") private final String am01ReasonLastCardRequest;
    @JsonProperty("UPDATE_TS") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") private final LocalDateTime updateTs = LocalDateTime.now();

    @Override
    public Object getPrimaryKey() {
        return mastAccountId;
    }
}
