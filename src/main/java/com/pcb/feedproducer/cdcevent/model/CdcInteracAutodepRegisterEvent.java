package com.pcb.feedproducer.cdcevent.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pcb.feedproducer.cdcevent.model.annotation.PrimaryKey;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDateTime;

@Jacksonized
@Builder
@Data
public class CdcInteracAutodepRegisterEvent implements CdcEvent<CdcInteracAutodepRegisterEvent> {
    @JsonProperty("INTERAC_AUTODEP_REGISTER_UID") @PrimaryKey private final Integer interacAutodepRegisterUid;
    @JsonProperty("ACCOUNT_UID") private final Integer accountUid;
    @JsonProperty("CUSTOMER_UID") private final Integer customerUid;
    @JsonProperty("PHONE_NUMBER") private final String phoneNumber;
    @JsonProperty("EMAIL_ADDRESS") private final String emailAddress;
    @JsonProperty("PCB_ISSUED_AUTODEP_REG_ID") private final String pcbIssuedAutodepRegId;
    @JsonProperty("INTERAC_ISSUED_AUTODEP_REG_ID") private final String interacIssuedAutodepRegId;
    @JsonProperty("ACTIVE_IND") private final Character activeInd;
    @JsonProperty("CREATE_DT") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSSSSSSS") private final LocalDateTime createDt;
    @JsonProperty("CREATE_USER_ID") private final String createUserId;
    @JsonProperty("CREATE_FUNCTION_NAME") private final String createFunctionName;
    @JsonProperty("UPDATE_DT") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSSSSSSS") private final LocalDateTime updateDt;
    @JsonProperty("UPDATE_USER_ID") private final String updateUserId;
    @JsonProperty("UPDATE_FUNCTION_NAME") private final String updateFunctionName;

    @Override
    public Object getPrimaryKey() {
        return interacAutodepRegisterUid;
    }
}
