package com.pcb.feedproducer.cdcevent.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.pcb.feedproducer.cdcevent.model.annotation.PrimaryKey;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDateTime;

@Jacksonized
@Builder
@Data
public class CdcAccountIdentifierEvent implements CdcEvent<CdcAccountIdentifierEvent> {
    @JsonProperty("ACCOUNT_IDENTIFIER_UID") @PrimaryKey private final Integer accountIdentifierUid;
    @JsonProperty("ACCOUNT_UID") private final Integer accountUid;
    @JsonProperty("TYPE") private final String type;
    @JsonProperty("QUALIFIER") private final String qualifier;
    @JsonProperty("ACCOUNT_IDENTIFIER_NO") private final String accountIdentifierNo;
    @JsonProperty("DISABLED_IND") private final Character disabledInd;
    @JsonProperty("CREATE_DT") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSSSSSSS") private final LocalDateTime createDt;
    @JsonProperty("CREATE_USER_ID") private final String createUserId;
    @JsonProperty("CREATE_FUNCTION_NAME") private final String createFunctionName;
    @JsonProperty("UPDATE_DT") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSSSSSSS") private final LocalDateTime updateDt;
    @JsonProperty("UPDATE_USER_ID") private final String updateUserId;
    @JsonProperty("UPDATE_FUNCTION_NAME") private final String updateFunctionName;

    @Override
    public Object getPrimaryKey() {
        return accountIdentifierUid;
    }
}
