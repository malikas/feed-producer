package com.pcb.feedproducer.cdcevent.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pcb.feedproducer.cdcevent.model.annotation.PrimaryKey;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDateTime;

@Jacksonized
@Builder
@Data
public class CdcAccessMediumEvent implements CdcEvent<CdcAccessMediumEvent> {
    @JsonProperty("ACCESS_MEDIUM_UID") @PrimaryKey private final Integer accessMediumUid;
    @JsonProperty("ACCOUNT_CUSTOMER_UID") private final Integer accountCustomerUid;
    @JsonProperty("ACCESS_MEDIUM_NO") private final String accessMediumNo;
    @JsonProperty("EXPIRY_DT") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") private final LocalDateTime expiryDt;
    @JsonProperty("CREATE_DT") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSSSSSSS") private final LocalDateTime createDt;
    @JsonProperty("CREATE_USER_ID") private final String createUserId;
    @JsonProperty("CREATE_FUNCTION_NAME") private final String createFunctionName;
    @JsonProperty("UPDATE_DT") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSSSSSSS") private final LocalDateTime updateDt;
    @JsonProperty("UPDATE_USER_ID") private final String updateUserId;
    @JsonProperty("UPDATE_FUNCTION_NAME") private final String updateFunctionName;
    @JsonProperty("DEACTIVATED_DT") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSSSSSSS") private final LocalDateTime deactivatedDt;
    @JsonProperty("ACTIVATION_DT") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSSSSSSS") private final LocalDateTime activationDt;
    @JsonProperty("MASKED_ACCESS_MEDIUM_NO") private final String maskedAccessMediumNo;
    @JsonProperty("ACCOUNT_RELATIONSHIP_STATUS") private final Character accountRelationshipStatus;
    @JsonProperty("TSYS_PRODUCT_CODE") private final String tsysProductCode;
    @JsonProperty("CLIENT_PRODUCT_CODE") private final String clientProductCode;

    @Override
    public Object getPrimaryKey() {
        return accessMediumUid;
    }
}
