package com.pcb.feedproducer.cdcevent.model.serde;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.pcb.feedproducer.cdcevent.model.CdcAccessMediumEvent;
import com.pcb.feedproducer.cdcevent.model.CdcAccountCustomerEvent;
import com.pcb.feedproducer.cdcevent.model.CdcAccountEvent;
import com.pcb.feedproducer.cdcevent.model.CdcAccountIdentifierEvent;
import com.pcb.feedproducer.cdcevent.model.CdcCustomerEvent;
import com.pcb.feedproducer.cdcevent.model.CdcCustomerIdentifierEvent;
import com.pcb.feedproducer.cdcevent.model.CdcEvent;
import com.pcb.feedproducer.cdcevent.model.CdcEventEnvelope;
import com.pcb.feedproducer.cdcevent.model.CdcInteracAutodepRegisterEvent;
import com.pcb.feedproducer.cdcevent.model.CdcTsysAm00Event;
import com.pcb.feedproducer.cdcevent.model.CdcTsysAm01Event;
import com.pcb.feedproducer.cdcevent.model.CdcTsysAm02Event;
import com.pcb.feedproducer.cdcevent.model.CdcTsysAm04Event;
import com.pcb.feedproducer.cdcevent.model.CdcTsysAm0aEvent;
import com.pcb.feedproducer.cdcevent.model.CdcTsysAm0eEvent;
import com.pcb.feedproducer.util.JacksonMapperFactory;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

@Slf4j
public class CdcMessageEnvelopeDeserializer extends JsonDeserializer<CdcEventEnvelope<?>> {
    private static final DateTimeFormatter NON_ISO_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSSSSS");
    private static final Map<String, String> TSYS_SEGMENT_TO_TABLE_NAME = Map.of(
            "am00", "TSYS_PCB_MC_ACCTMASTER_DAILY_AM00",
            "am01", "TSYS_PCB_MC_ACCTMASTER_DAILY_AM01",
            "am02", "TSYS_PCB_MC_ACCTMASTER_DAILY_AM02",
            "am04", "TSYS_PCB_MC_ACCTMASTER_DAILY_AM04",
            "am0a", "TSYS_PCB_MC_ACCTMASTER_DAILY_AM0A",
            "am0e", "TSYS_PCB_MC_ACCTMASTER_DAILY_AM0E"
    );
    private static final Map<String, Class<?>> CDC_EVENT_TYPE_BY_TABLE_NAME = Map.ofEntries(
            Map.entry("ACCOUNT", CdcAccountEvent.class),
            Map.entry("CUSTOMER", CdcCustomerEvent.class),
            Map.entry("ACCOUNT_CUSTOMER", CdcAccountCustomerEvent.class),
            Map.entry("ACCESS_MEDIUM", CdcAccessMediumEvent.class),
            Map.entry("ACCOUNT_IDENTIFIER", CdcAccountIdentifierEvent.class),
            Map.entry("CUSTOMER_IDENTIFIER", CdcCustomerIdentifierEvent.class),
            Map.entry("INTERAC_AUTODEP_REGISTER", CdcInteracAutodepRegisterEvent.class),
            Map.entry("TSYS_PCB_MC_ACCTMASTER_DAILY_AM00", CdcTsysAm00Event.class),
            Map.entry("TSYS_PCB_MC_ACCTMASTER_DAILY_AM01", CdcTsysAm01Event.class),
            Map.entry("TSYS_PCB_MC_ACCTMASTER_DAILY_AM02", CdcTsysAm02Event.class),
            Map.entry("TSYS_PCB_MC_ACCTMASTER_DAILY_AM04", CdcTsysAm04Event.class),
            Map.entry("TSYS_PCB_MC_ACCTMASTER_DAILY_AM0A", CdcTsysAm0aEvent.class),
            Map.entry("TSYS_PCB_MC_ACCTMASTER_DAILY_AM0E", CdcTsysAm0eEvent.class)
    );

    @Override
    public CdcEventEnvelope<?> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        try {
            JsonNode node = jsonParser.getCodec().readTree(jsonParser);

            var table = node.has("table") ? node.get("table").asText() : TSYS_SEGMENT_TO_TABLE_NAME.get(node.get("segment").asText());
            var opType = node.has("op_type") ? CdcEventEnvelope.OperationType.fromString(node.get("op_type").asText()) : CdcEventEnvelope.OperationType.U;
            var opTs = LocalDateTime.parse(node.get("op_ts").asText(), NON_ISO_DATE_TIME_FORMATTER);
            var currentTs = node.has("current_ts") ? LocalDateTime.parse(node.get("current_ts").asText()) : opTs;
            var pos = node.has("pos") ? node.get("pos").asText() : "";
            var beforeEvent = node.get("before");
            var afterEvent = node.get("after");

            var normalizedTableName = table.contains(".") ? table.split("\\.")[1] : table;

            return CdcEventEnvelope.builder()
                    .table(table)
                    .opType(opType)
                    .opTs(opTs)
                    .currentTs(currentTs)
                    .pos(pos)
                    .beforeEvent(beforeEvent != null ? JacksonMapperFactory.getMapper().treeToValue(beforeEvent, CDC_EVENT_TYPE_BY_TABLE_NAME.getOrDefault(normalizedTableName, CdcEvent.class)) : null)
                    .afterEvent(afterEvent != null ? JacksonMapperFactory.getMapper().treeToValue(afterEvent, CDC_EVENT_TYPE_BY_TABLE_NAME.getOrDefault(normalizedTableName, CdcEvent.class)) : null)
                    .build();
        } catch (IOException e) {
            log.error("Error occurred while deserializing CdcEventEnvelope", e);
            throw e;
        }
    }
}
