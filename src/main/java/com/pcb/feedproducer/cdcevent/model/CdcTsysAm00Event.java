package com.pcb.feedproducer.cdcevent.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDateTime;

@Jacksonized
@Builder
@Data
public class CdcTsysAm00Event implements CdcEvent<CdcTsysAm00Event> {
    @JsonProperty("MAST_ACCOUNT_ID") private final String mastAccountId;
    @JsonProperty("AM00_STATF_CRV") private final String am00StatfCrv;
    @JsonProperty("AM00_STATF_REINSTATE_CHARGEOFF") private final String am00StatfReinstateChargeoff;
    @JsonProperty("AM00_STATC_CURRENT_OVERLIMIT") private final String am00StatcCurrentOverlimit;
    @JsonProperty("AM00_STATC_WATCH") private final String am00StatcWatch;
    @JsonProperty("AM00_STATC_TIERED_AUTH") private final String am00StatcTieredAuth;
    @JsonProperty("AM00_STATF_CREDIT_COUNSELING") private final String am00StatfCreditCounseling;
    @JsonProperty("AM00_BANKRUPTCY_TYPE") private final String am00BankruptcyType;
    @JsonProperty("AM00_STATC_CHARGEOFF") private final String am00StatcChargeoff;
    @JsonProperty("AM00_STATC_CREDIT_REVOKED") private final String am00StatcCreditRevoked;
    @JsonProperty("AM00_STATC_CLOSED") private final String am00StatcClosed;
    @JsonProperty("AM00_STATF_FRAUD") private final String am00StatfFraud;
    @JsonProperty("AM00_LIMIT_CREDIT") private final String am00LimitCredit;
    @JsonProperty("AM00_TYPEC_VIP") private final String am00TypecVip;
    @JsonProperty("AM00_DATE_LAST_BILLING") private final String am00DateLastBilling;
    @JsonProperty("AM00_STATC_CURRENT_PAST_DUE") private final String am00StatcCurrentPastDue;
    @JsonProperty("AM00_CUSTOM_DATA_81") private final String am00CustomData81;
    @JsonProperty("AM00_STATC_SECURITY_FRAUD") private final String am00StatcSecurityFraud;
    @JsonProperty("AM00_STATF_PRODUCT_CHANGE") private final String am00StatfProductChange;
    @JsonProperty("UPDATE_TS") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") private final LocalDateTime updateTs = LocalDateTime.now();

    @Override
    public Object getPrimaryKey() {
        return mastAccountId;
    }
}
