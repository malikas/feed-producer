package com.pcb.feedproducer.cdcevent.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pcb.feedproducer.cdcevent.model.annotation.PrimaryKey;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDateTime;

@Jacksonized
@Builder
@Data
public class CdcAccountEvent implements CdcEvent<CdcAccountEvent> {
    @JsonProperty("ACCOUNT_UID") @PrimaryKey private final Integer accountUid;
    @JsonProperty("ACCOUNT_NO") private final String accountNo;
    @JsonProperty("CREATE_DT") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSSSSSSS") private final LocalDateTime createDt;
    @JsonProperty("CREATE_USER_ID") private final String createUserId;
    @JsonProperty("CREATE_FUNCTION_NAME") private final String createFunctionName;
    @JsonProperty("UPDATE_DT") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSSSSSSS") private final LocalDateTime updateDt;
    @JsonProperty("UPDATE_USER_ID") private final String updateUserId;
    @JsonProperty("UPDATE_FUNCTION_NAME") private final String updateFunctionName;
    @JsonProperty("PRODUCT_UID") private final Integer productId;
    @JsonProperty("TYPE") private final String type;
    @JsonProperty("OPEN_DT") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") private final LocalDateTime openDt;
    @JsonProperty("E_DELIVERY_IND") private final Character eDeliveryInd;
    @JsonProperty("E_DELIVERY_UPDATE_DATE") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSSSSSSS") private final LocalDateTime eDeliveryUpdateDate;
    @JsonProperty("INACTIVE_CYCLES") private final Integer inactiveCycles;

    @Override
    public Object getPrimaryKey() {
        return accountUid;
    }
}
