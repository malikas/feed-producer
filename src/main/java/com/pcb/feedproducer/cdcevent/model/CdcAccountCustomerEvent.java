package com.pcb.feedproducer.cdcevent.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.pcb.feedproducer.cdcevent.model.annotation.PrimaryKey;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Jacksonized
@Builder
@Data
public class CdcAccountCustomerEvent implements CdcEvent<CdcAccountCustomerEvent> {
    @JsonProperty("ACCOUNT_CUSTOMER_UID") @PrimaryKey private final Integer accountCustomerUid;
    @JsonProperty("ACCOUNT_UID") private final Integer accountUid;
    @JsonProperty("CUSTOMER_UID") private final Integer customerUid;
    @JsonProperty("CREATE_DT") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSSSSSSS") private LocalDateTime createDt;
    @JsonProperty("CREATE_USER_ID") private final String createUserId;
    @JsonProperty("CREATE_FUNCTION_NAME") private final String createFunctionName;
    @JsonProperty("UPDATE_DT") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSSSSSSS") private LocalDateTime updateDt;
    @JsonProperty("UPDATE_USER_ID") private final String updateUserId;
    @JsonProperty("UPDATE_FUNCTION_NAME") private final String updateFunctionName;
    @JsonProperty("ACCOUNT_CUSTOMER_ROLE_UID") private final Integer accountCustomerRoleUid;
    @JsonProperty("ACCOUNT_CUSTOMER_NO") private final String accountCustomerNo;
    @JsonProperty("ACTIVE_IND") private final Character activeInd;
    @JsonProperty("DEACTIVATED_ON") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSSSSSSS") private LocalDateTime deactivatedOn;
    @JsonProperty("OPEN_DT") @JsonFormat(pattern = "yyyy-MM-dd") private LocalDate openDate;

    @Override
    public Object getPrimaryKey() {
        return accountCustomerUid;
    }
}
