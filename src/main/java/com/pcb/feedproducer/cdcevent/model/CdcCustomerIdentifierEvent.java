package com.pcb.feedproducer.cdcevent.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pcb.feedproducer.cdcevent.model.annotation.PrimaryKey;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDateTime;

@Jacksonized
@Builder
@Data
public class CdcCustomerIdentifierEvent implements CdcEvent<CdcCustomerIdentifierEvent> {
    @JsonProperty("CUSTOMER_IDENTIFIER_UID") @PrimaryKey private final Integer customerIdentifierUid;
    @JsonProperty("CUSTOMER_IDENTIFIER_NO") private final String customerIdentifierNo;
    @JsonProperty("TYPE") private final String type;
    @JsonProperty("CUSTOMER_UID") private final Integer customerUid;
    @JsonProperty("CREATE_DT") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSSSSSSS") private final LocalDateTime createDt;
    @JsonProperty("CREATE_USER_ID") private final String createUserId;
    @JsonProperty("CREATE_FUNCTION_NAME") private final String createFunctionName;
    @JsonProperty("UPDATE_DT") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSSSSSSS") private final LocalDateTime updateDt;
    @JsonProperty("UPDATE_USER_ID") private final String updateUserId;
    @JsonProperty("UPDATE_FUNCTION_NAME") private final String updateFunctionName;
    @JsonProperty("DISABLED_IND") private final Character disabledInd;

    @Override
    public Object getPrimaryKey() {
        return customerIdentifierUid;
    }
}
