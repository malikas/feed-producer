package com.pcb.feedproducer.cdcevent.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Jacksonized
@Builder
@Data
public class CdcCustomerEvent implements CdcEvent<CdcCustomerEvent> {
    @JsonProperty("CUSTOMER_UID") private final Integer customerUid;
    @JsonProperty("PLATFORM_USER_UID") private final Integer platformUserUid;
    @JsonProperty("GIVEN_NAME") private final String givenName;
    @JsonProperty("SURNAME") private final String surname;
    @JsonProperty("CREATE_DT") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSSSSSSS") private LocalDateTime createDt;
    @JsonProperty("CREATE_USER_ID") private final String createUserId;
    @JsonProperty("CREATE_FUNCTION_NAME") private final String createFunctionName;
    @JsonProperty("UPDATE_DT") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSSSSSSS") private LocalDateTime updateDt;
    @JsonProperty("UPDATE_USER_ID") private final String updateUserId;
    @JsonProperty("UPDATE_FUNCTION_NAME") private final String updateFunctionName;
    @JsonProperty("BIRTH_DT") @JsonFormat(pattern = "yyyy-MM-dd") private LocalDate birthDate;
    @JsonProperty("NAME_PREFIX") private final String namePrefix;
    @JsonProperty("NAME_SUFFIX") private final String nameSuffix;
    @JsonProperty("MIDDLE_NAME") private final String middleName;
    @JsonProperty("MESSAGE_ID") private final String messageId;
    @JsonProperty("KYC_ASSESSED_ON") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSSSSSSSS") private LocalDateTime keyAssessedOn;
    @JsonProperty("FIRST_TIME_LOGIN_COMPLETED_IND") private final Character firstTimeLoginCompletedInd;
    @JsonProperty("INACTIVE_REASON_CODE") private final String inactiveReasonCode;

    @Override
    public Object getPrimaryKey() {
        return customerUid;
    }
}
