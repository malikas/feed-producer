package com.pcb.feedproducer.cdcevent.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.extern.jackson.Jacksonized;

import java.time.LocalDateTime;

@Jacksonized
@Builder
@Data
public class CdcTsysAm04Event implements CdcEvent<CdcTsysAm04Event> {
    @JsonProperty("MAST_ACCOUNT_ID") private final String mastAccountId;
    @JsonProperty("AM04_AMT_PASTDUE_1") private final String am04AmtPastdue1;
    @JsonProperty("AM04_AMT_PASTDUE_2") private final String am04AmtPastdue2;
    @JsonProperty("AM04_AMT_PASTDUE_3") private final String am04AmtPastdue3;
    @JsonProperty("AM04_AMT_PASTDUE_4") private final String am04AmtPastdue4;
    @JsonProperty("AM04_AMT_PASTDUE_5") private final String am04AmtPastdue5;
    @JsonProperty("AM04_AMT_PASTDUE_6") private final String am04AmtPastdue6;
    @JsonProperty("AM04_AMT_PASTDUE_7") private final String am04AmtPastdue7;
    @JsonProperty("AM04_AMT_PASTDUE_8") private final String am04AmtPastdue8;
    @JsonProperty("AM04_AMT_PASTDUE_9") private final String am04AmtPastdue9;
    @JsonProperty("AM04_AMT_PASTDUE_10") private final String am04AmtPastdue10;
    @JsonProperty("AM04_AMT_PASTDUE_11") private final String am04AmtPastdue11;
    @JsonProperty("AM04_AMT_PASTDUE_12") private final String am04AmtPastdue12;
    @JsonProperty("UPDATE_TS") @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss") private final LocalDateTime updateTs = LocalDateTime.now();

    @Override
    public Object getPrimaryKey() {
        return mastAccountId;
    }
}
