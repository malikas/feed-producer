package com.pcb.feedproducer.cdcevent.worker.kafka;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.pcb.feedproducer.cdcevent.model.CdcEvent;
import com.pcb.feedproducer.cdcevent.model.CdcEventEnvelope;
import com.pcb.feedproducer.cdcevent.processor.CdcEventProcessor;
import com.pcb.feedproducer.cdcevent.worker.CdcEventConsumerWorker;
import com.pcb.feedproducer.feed.FeedProcessor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.Objects;

@Service
@Slf4j
public class KafkaCdcEventConsumerWorker implements CdcEventConsumerWorker<String, CdcEventEnvelope<? extends CdcEvent<?>>> {
    private final CdcEventProcessor cdcEventProcessor;
    private final FeedProcessor feedProcessor;

    private final Timer cdcEventEndToEndProcessingTimer;

    public KafkaCdcEventConsumerWorker(MetricRegistry metricRegistry, CdcEventProcessor cdcEventProcessor, FeedProcessor feedProcessor) {
        this.cdcEventProcessor = Objects.requireNonNull(cdcEventProcessor);
        this.feedProcessor = Objects.requireNonNull(feedProcessor);

        this.cdcEventEndToEndProcessingTimer = metricRegistry.timer("cdcEventEndToEndProcessingTimer");
    }

    @Override
    @KafkaListener(topics = "#{'${KafkaCdcEventConsumerWorker.topicNames}'.split(',')}",
            containerFactory = "cdcEventKafkaListenerContainerFactory",
            concurrency = "${KafkaCdcEventConsumerWorker.concurrency}",
            properties = {"spring.json.value.default.type=com.pcb.feedproducer.cdcevent.model.CdcEventEnvelope"})
    public void consumeCdcEventRecord(ConsumerRecord<String, CdcEventEnvelope<? extends CdcEvent<?>>> consumerRecord, Acknowledgment ack) {
        CdcEventEnvelope<? extends CdcEvent<?>> cdcEventEnvelope = consumerRecord.value();

        cdcEventEndToEndProcessingTimer.time(() -> {
            log.info("Received CdcEvent: {}", cdcEventEnvelope);
            handleCdcEvent(cdcEventEnvelope);
            ack.acknowledge();
            log.info("Successfully processed CdcEvent: {}", cdcEventEnvelope);
        });
    }

    @Override
    public void handleCdcEvent(CdcEventEnvelope<? extends CdcEvent<?>> cdcEventEnvelope) {
        Objects.requireNonNull(cdcEventEnvelope);
        cdcEventProcessor.processCdcEvent(cdcEventEnvelope);
        feedProcessor.generateFeedsForCdcEvent(cdcEventEnvelope);
    }

    @PreDestroy
    public void preDestroy() {
        log.info("Shutting down KafkaCdcEventConsumerWorker");
    }
}