package com.pcb.feedproducer.cdcevent.worker.kafka;

import com.google.common.collect.ImmutableSet;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ConsumerAwareRecordRecoverer;
import org.springframework.kafka.listener.ConsumerRecordRecoverer;
import org.springframework.kafka.listener.DefaultErrorHandler;
import org.springframework.kafka.support.converter.ConversionException;
import org.springframework.kafka.support.serializer.DeserializationException;
import org.springframework.messaging.converter.MessageConversionException;
import org.springframework.messaging.handler.invocation.MethodArgumentResolutionException;
import org.springframework.util.backoff.ExponentialBackOff;

import java.util.Objects;
import java.util.Set;

@Configuration
@EnableConfigurationProperties(KafkaProperties.class)
@Slf4j
public class KafkaCdcEventConsumerWorkerConfig {
    private static final Set<Class<? extends Throwable>> IRRECOVERABLE_EXCEPTIONS = ImmutableSet.of(
            DeserializationException.class,
            MessageConversionException.class,
            ConversionException.class,
            MethodArgumentResolutionException.class,
            NoSuchMethodException.class,
            ClassCastException.class);

    @Value("${KafkaCdcEventConsumerWorkerConfig.retryInitialIntervalInMillis:2000}") private Long retryInitialIntervalInMillis;
    @Value("${KafkaCdcEventConsumerWorkerConfig.retryMultiplier:2}") private Long retryMultiplier;
    @Value("${KafkaCdcEventConsumerWorkerConfig.retryMaxElapsedTimeInMillis:15000}") private Long retryMaxElapsedTimeInMillis;

    private final KafkaProperties kafkaProperties;

    public KafkaCdcEventConsumerWorkerConfig(KafkaProperties kafkaProperties) {
        this.kafkaProperties = Objects.requireNonNull(kafkaProperties);
    }

    @Bean(name = "cdcEventKafkaListenerContainerFactory")
    public <V> ConcurrentKafkaListenerContainerFactory<String, V> cdcEventKafkaListenerContainerFactory(ConsumerRecordRecoverer consumerRecordRecoverer) {
        ConcurrentKafkaListenerContainerFactory<String, V> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.getContainerProperties().setAckMode(kafkaProperties.getListener().getAckMode());
        factory.setConsumerFactory(new DefaultKafkaConsumerFactory<>(kafkaProperties.buildConsumerProperties()));
        var exponentialBackOff = new ExponentialBackOff();
        exponentialBackOff.setInitialInterval(retryInitialIntervalInMillis);
        exponentialBackOff.setMultiplier(retryMultiplier);
        exponentialBackOff.setMaxElapsedTime(retryMaxElapsedTimeInMillis);
        factory.setCommonErrorHandler(new DefaultErrorHandler(consumerRecordRecoverer, exponentialBackOff));
        return factory;
    }

    @Bean
    public ConsumerRecordRecoverer consumerRecordRecoverer() {
        return (ConsumerAwareRecordRecoverer) (consumerRecord, consumer, thrownException) -> {
            var exception = thrownException.getCause() != null ? thrownException.getCause().getClass() : thrownException.getClass();
            if (IRRECOVERABLE_EXCEPTIONS.contains(exception)) {
                log.error("Error(IRRECOVERABLE) occurred while processing record: {}-{}@{}",
                        consumerRecord.topic(), consumerRecord.partition(), consumerRecord.offset(), thrownException);
            } else {
                log.error("Error occurred while processing record: {}-{}@{}. Giving up after a number of retries and discarding the record.",
                        consumerRecord.topic(), consumerRecord.partition(), consumerRecord.offset(), thrownException);
            }
            if (consumer != null) {
                consumer.commitSync();
            }
        };
    }
}
