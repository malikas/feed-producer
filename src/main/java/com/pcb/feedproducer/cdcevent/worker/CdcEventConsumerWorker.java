package com.pcb.feedproducer.cdcevent.worker;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.support.Acknowledgment;

public interface CdcEventConsumerWorker<K, V> {
    void consumeCdcEventRecord(ConsumerRecord<K, V> consumerRecord, Acknowledgment acknowledgment);

    void handleCdcEvent(V cdcEvent);
}
