package com.pcb.feedproducer.cdcevent.persistence.exception;

public class FeedDetailsException extends RuntimeException {
    public FeedDetailsException(String message) {
        super(message);
    }

    public FeedDetailsException(Throwable cause) {
        super(cause);
    }

    public FeedDetailsException(String message, Throwable cause) {
        super(message, cause);
    }
}
