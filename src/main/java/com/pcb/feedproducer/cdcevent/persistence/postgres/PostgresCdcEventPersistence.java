package com.pcb.feedproducer.cdcevent.persistence.postgres;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.pcb.feedproducer.cdcevent.model.CdcAccessMediumEvent;
import com.pcb.feedproducer.cdcevent.model.CdcAccountCustomerEvent;
import com.pcb.feedproducer.cdcevent.model.CdcAccountEvent;
import com.pcb.feedproducer.cdcevent.model.CdcAccountIdentifierEvent;
import com.pcb.feedproducer.cdcevent.model.CdcCustomerEvent;
import com.pcb.feedproducer.cdcevent.model.CdcCustomerIdentifierEvent;
import com.pcb.feedproducer.cdcevent.model.CdcEvent;
import com.pcb.feedproducer.cdcevent.model.CdcEventEnvelope;
import com.pcb.feedproducer.cdcevent.model.CdcInteracAutodepRegisterEvent;
import com.pcb.feedproducer.cdcevent.model.CdcTsysAm00Event;
import com.pcb.feedproducer.cdcevent.model.CdcTsysAm01Event;
import com.pcb.feedproducer.cdcevent.model.CdcTsysAm02Event;
import com.pcb.feedproducer.cdcevent.model.CdcTsysAm04Event;
import com.pcb.feedproducer.cdcevent.model.CdcTsysAm0aEvent;
import com.pcb.feedproducer.cdcevent.model.CdcTsysAm0eEvent;
import com.pcb.feedproducer.cdcevent.model.annotation.PrimaryKey;
import com.pcb.feedproducer.cdcevent.persistence.CdcEventPersistence;
import com.pcb.feedproducer.cdcevent.persistence.exception.CdcEventPersistenceException;
import com.pcb.feedproducer.cdcevent.persistence.exception.FeedDetailsException;
import com.pcb.feedproducer.feed.model.FeedDetails;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.stereotype.Repository;

import java.lang.reflect.Field;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.pcb.feedproducer.feed.query.FeedQueryUtil.POSTGRES_SELECT_FEED_DETAILS_WHERE_ACCESS_MEDIUM_UID_PRIMARY_KEY_QUERY;
import static com.pcb.feedproducer.feed.query.FeedQueryUtil.POSTGRES_SELECT_FEED_DETAILS_WHERE_ACCOUNT_UID_PRIMARY_KEY_QUERY;
import static com.pcb.feedproducer.feed.query.FeedQueryUtil.POSTGRES_SELECT_FEED_DETAILS_WHERE_CUSTOMER_IDENTIFIER_UID_PRIMARY_KEY_QUERY;
import static com.pcb.feedproducer.feed.query.FeedQueryUtil.POSTGRES_SELECT_FEED_DETAILS_WHERE_CUSTOMER_UID_PRIMARY_KEY_QUERY;
import static com.pcb.feedproducer.feed.query.FeedQueryUtil.POSTGRES_SELECT_FEED_DETAILS_WHERE_MAST_ACCOUNT_ID_PRIMARY_KEY_QUERY;

@Repository
@Slf4j
public class PostgresCdcEventPersistence implements CdcEventPersistence {
    private static final String TABLE_NAME_MACRO = "$TABLE_NAME$";
    private static final String COLUMN_NAMES_MACRO = "$COLUMN_NAMES$";
    private static final String COLUMN_VALUES_MACRO = "$COLUMN_VALUES$";
    private static final String UPDATE_COLUMNS_MACRO = "$UPDATE_COLUMNS$";
    private static final String PRIMARY_KEY_MACRO = "$PRIMARY_KEY$";
    private static final String INSERT_QUERY_TEMPLATE = "INSERT INTO " + TABLE_NAME_MACRO + " (" + COLUMN_NAMES_MACRO + ") "
            + "VALUES (" + COLUMN_VALUES_MACRO + ") ON CONFLICT (" + PRIMARY_KEY_MACRO + ") DO UPDATE SET " + UPDATE_COLUMNS_MACRO;

    private static final Map<Class<? extends CdcEvent<?>>, String> CDC_EVENT_TO_QUERY_MAPPING = Map.ofEntries(
            Map.entry(CdcAccountEvent.class, POSTGRES_SELECT_FEED_DETAILS_WHERE_ACCOUNT_UID_PRIMARY_KEY_QUERY),
            Map.entry(CdcCustomerEvent.class, POSTGRES_SELECT_FEED_DETAILS_WHERE_CUSTOMER_UID_PRIMARY_KEY_QUERY),
            Map.entry(CdcAccountCustomerEvent.class, POSTGRES_SELECT_FEED_DETAILS_WHERE_MAST_ACCOUNT_ID_PRIMARY_KEY_QUERY),
            Map.entry(CdcAccessMediumEvent.class, POSTGRES_SELECT_FEED_DETAILS_WHERE_ACCESS_MEDIUM_UID_PRIMARY_KEY_QUERY),
            Map.entry(CdcAccountIdentifierEvent.class, POSTGRES_SELECT_FEED_DETAILS_WHERE_ACCOUNT_UID_PRIMARY_KEY_QUERY),
            Map.entry(CdcCustomerIdentifierEvent.class, POSTGRES_SELECT_FEED_DETAILS_WHERE_CUSTOMER_IDENTIFIER_UID_PRIMARY_KEY_QUERY),
            Map.entry(CdcInteracAutodepRegisterEvent.class, POSTGRES_SELECT_FEED_DETAILS_WHERE_ACCOUNT_UID_PRIMARY_KEY_QUERY),
            Map.entry(CdcTsysAm00Event.class, POSTGRES_SELECT_FEED_DETAILS_WHERE_MAST_ACCOUNT_ID_PRIMARY_KEY_QUERY),
            Map.entry(CdcTsysAm01Event.class, POSTGRES_SELECT_FEED_DETAILS_WHERE_MAST_ACCOUNT_ID_PRIMARY_KEY_QUERY),
            Map.entry(CdcTsysAm02Event.class, POSTGRES_SELECT_FEED_DETAILS_WHERE_MAST_ACCOUNT_ID_PRIMARY_KEY_QUERY),
            Map.entry(CdcTsysAm04Event.class, POSTGRES_SELECT_FEED_DETAILS_WHERE_MAST_ACCOUNT_ID_PRIMARY_KEY_QUERY),
            Map.entry(CdcTsysAm0aEvent.class, POSTGRES_SELECT_FEED_DETAILS_WHERE_MAST_ACCOUNT_ID_PRIMARY_KEY_QUERY),
            Map.entry(CdcTsysAm0eEvent.class, POSTGRES_SELECT_FEED_DETAILS_WHERE_MAST_ACCOUNT_ID_PRIMARY_KEY_QUERY)
    );

    private final JdbcTemplate jdbcTemplate;

    private final Timer persistCdcEventTimer;
    private final Timer fetchFeedDetailsTimer;

    @Autowired
    public PostgresCdcEventPersistence(MetricRegistry metricRegistry, JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = Objects.requireNonNull(jdbcTemplate);

        this.persistCdcEventTimer = metricRegistry.timer("persistCdcEventTimer");
        this.fetchFeedDetailsTimer = metricRegistry.timer("fetchFeedDetailsTimer");
    }

    @Override
    public void persistCdcEvent(CdcEventEnvelope<? extends CdcEvent<?>> cdcEventEnvelope) {
        Objects.requireNonNull(cdcEventEnvelope);
        log.debug("Persisting CdcEvent: ({}) to Postgres data store", cdcEventEnvelope);

        try {
            var cdcEvent = cdcEventEnvelope.getAfterEvent();
            var fieldsFromCdcEvent = getFieldsFromCdcEvent(cdcEvent);
            var insertQuery = generateInsertQuery(cdcEventEnvelope, fieldsFromCdcEvent);

            var preparedStatementCreator = constructPreparedStatementCreatorForInsert(insertQuery, fieldsFromCdcEvent, cdcEvent);
            var wasInsertSuccessful = persistCdcEventTimer.time(() -> jdbcTemplate.update(preparedStatementCreator));

            if (wasInsertSuccessful > 0) {
                log.debug("CdcEvent was persisted successfully");
            } else {
                throw new RuntimeException("CdcEvent failed to be persisted");
            }
        } catch (Exception e) {
            throw new CdcEventPersistenceException("Error occurred while persisting CdcEvent", e);
        }
    }

    @Override
    public List<FeedDetails> fetchFeedDetails(CdcEventEnvelope<? extends CdcEvent<?>> cdcEventEnvelope) {
        Objects.requireNonNull(cdcEventEnvelope);
        log.debug("Fetching FeedDetails for CdcEvent: {} from Postgres data store", cdcEventEnvelope);

        try {
            var primaryKey = cdcEventEnvelope.getAfterEvent().getPrimaryKey();
            var feedDetails = fetchFeedDetailsTimer.time(() -> jdbcTemplate.query(CDC_EVENT_TO_QUERY_MAPPING.get(cdcEventEnvelope.getAfterEvent().getClass()), (rs, rowNum) -> FeedDetails.builder()
                    .customerAcctNumber(rs.getString("customerAcctNumber"))
                    .productId(rs.getInt("productId"))
                    .numberOfCyclesInactive(rs.getInt("numberOfCyclesInactive"))
                    .openDate(rs.getTimestamp("openDate").toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
                    .limitCredit(rs.getString("limitCredit"))
                    .statcCurrentPastDue(rs.getString("statcCurrentPastDue"))
                    .customData81(rs.getString("customData81"))
                    .dateLastBilling(rs.getString("dateLastBilling"))
                    .statcClosed(rs.getString("statcClosed"))
                    .statcSecurityFraud(rs.getString("statcSecurityFraud"))
                    .bankruptcyType(rs.getString("bankruptcyType"))
                    .statcCreditRevoked(rs.getString("statcCreditRevoked"))
                    .statcChargeoff(rs.getString("statcChargeoff"))
                    .statfReinstateChargeoff(rs.getString("statfReinstateChargeoff"))
                    .statcCurrentOverlimit(rs.getString("statcCurrentOverlimit"))
                    .statcWatch(rs.getString("statcWatch"))
                    .statcTieredAuth(rs.getString("statcTieredAuth"))
                    .statfCreditCounseling(rs.getString("statfCreditCounseling"))
                    .statfCrv(rs.getString("statfCrv"))
                    .cardReceiptVerifyInd(rs.getString("cardReceiptVerifyInd"))
                    .dateLastPinChange(rs.getString("dateLastPinChange"))
                    .dateAccountValidFrom(rs.getString("dateAccountValidFrom"))
                    .dateLastCardRequest(rs.getString("dateLastCardRequest"))
                    .reasonLastCardRequest(rs.getString("reasonLastCardRequest"))
                    .balanceCurrent(rs.getString("balanceCurrent"))
                    .delinquentAmount(rs.getString("delinquentAmount"))
                    .statcCardWatch(rs.getString("statcCardWatch"))
                    .nameEmboss(rs.getString("nameEmboss"))
                    .customerIdFromHeader(rs.getString("customerIdFromHeader"))
                    .jointCustomerId(rs.getString("jointCustomerId"))
                    .numberOfPaymentIds(rs.getInt("numberOfPaymentIds"))
                    .numberOfAuthorizedUsers(rs.getInt("numberOfAuthorizedUsers"))
                    .userIndicator01(rs.getString("userIndicator01"))
                    .accountIdentifierNo(rs.getString("accountIdentifierNo"))
                    .build(), primaryKey));

            log.debug("Successfully fetched FeedDetails: {}", feedDetails);
            return feedDetails;
        } catch (Exception e) {
            throw new FeedDetailsException("Error occurred while fetching FeedDetails", e);
        }
    }

    private PreparedStatementCreator constructPreparedStatementCreatorForInsert(String insertQuery, List<Field> fieldsFromCdcEvent, CdcEvent<?> cdcEvent) {
        return connection -> {
            try {
                PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);

                int index = 1;
                for (int i = 0; i < 2; i++) {
                    for (Field field : fieldsFromCdcEvent) {
                        field.setAccessible(true);
                        Object value = field.get(cdcEvent);
                        setValueOntoPreparedStatement(preparedStatement, index++, field, value);
                    }
                }
                return preparedStatement;
            } catch (IllegalAccessException e) {
                throw new RuntimeException("Error occurred while bootstrapping preparedStatement", e);
            }
        };
    }

    private List<Field> getFieldsFromCdcEvent(CdcEvent<?> cdcEvent) {
        List<Field> fields;
        if (cdcEvent instanceof CdcAccountEvent) {
            fields = Arrays.asList(CdcAccountEvent.class.getDeclaredFields());
        } else if (cdcEvent instanceof CdcCustomerEvent) {
            fields = Arrays.asList(CdcCustomerEvent.class.getDeclaredFields());
        } else if (cdcEvent instanceof CdcAccountCustomerEvent) {
            fields = Arrays.asList(CdcAccountCustomerEvent.class.getDeclaredFields());
        } else if (cdcEvent instanceof CdcAccessMediumEvent) {
            fields = Arrays.asList(CdcAccessMediumEvent.class.getDeclaredFields());
        } else if (cdcEvent instanceof CdcAccountIdentifierEvent) {
            fields = Arrays.asList(CdcAccountIdentifierEvent.class.getDeclaredFields());
        } else if (cdcEvent instanceof CdcCustomerIdentifierEvent) {
            fields = Arrays.asList(CdcCustomerIdentifierEvent.class.getDeclaredFields());
        } else if (cdcEvent instanceof CdcInteracAutodepRegisterEvent) {
            fields = Arrays.asList(CdcInteracAutodepRegisterEvent.class.getDeclaredFields());
        } else if (cdcEvent instanceof CdcTsysAm00Event) {
            fields = Arrays.asList(CdcTsysAm00Event.class.getDeclaredFields());
        } else if (cdcEvent instanceof CdcTsysAm01Event) {
            fields = Arrays.asList(CdcTsysAm01Event.class.getDeclaredFields());
        } else if (cdcEvent instanceof CdcTsysAm02Event) {
            fields = Arrays.asList(CdcTsysAm02Event.class.getDeclaredFields());
        } else if (cdcEvent instanceof CdcTsysAm04Event) {
            fields = Arrays.asList(CdcTsysAm04Event.class.getDeclaredFields());
        } else if (cdcEvent instanceof CdcTsysAm0aEvent) {
            fields = Arrays.asList(CdcTsysAm0aEvent.class.getDeclaredFields());
        } else if (cdcEvent instanceof CdcTsysAm0eEvent) {
            fields = Arrays.asList(CdcTsysAm0eEvent.class.getDeclaredFields());
        } else {
            throw new UnsupportedOperationException("Unsupported CdcEvent provided: " + cdcEvent.getClass().getSimpleName());
        }
        return fields;
    }

    private String generateInsertQuery(CdcEventEnvelope<? extends CdcEvent<?>> cdcEventEnvelope, List<Field> fields) {
        var primaryKeyField = fields
                .stream()
                .filter(field -> field.isAnnotationPresent(PrimaryKey.class))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("No PrimaryKey annotation is defined in the class"));
        var columnNames = fields
                .stream()
                .map(field -> field.getAnnotation(JsonProperty.class).value())
                .collect(Collectors.toList());
        var valuePlaceholders = StringUtils.repeat("?, ", columnNames.size());
        valuePlaceholders = valuePlaceholders.substring(0, valuePlaceholders.length() - 2);
        var updateClause = StringUtils.join(columnNames.stream().map(column -> column + " = ?").collect(Collectors.toList()), ", ");

        return INSERT_QUERY_TEMPLATE
                .replace(TABLE_NAME_MACRO, normalizeTableName(cdcEventEnvelope.getTable()))
                .replace(COLUMN_NAMES_MACRO, StringUtils.join(columnNames, ", "))
                .replace(COLUMN_VALUES_MACRO, valuePlaceholders)
                .replace(UPDATE_COLUMNS_MACRO, updateClause)
                .replace(PRIMARY_KEY_MACRO, primaryKeyField.getAnnotation(JsonProperty.class).value());
    }

    private void setValueOntoPreparedStatement(PreparedStatement ps, int index, Field cdcEventField, Object value) throws SQLException {
        var returnType = cdcEventField.getType();

        if (String.class.equals(returnType) || Enum.class.isAssignableFrom(returnType)) {
            String valueToInsert = null;
            if (value != null) {
                if (value instanceof Enum) {
                    valueToInsert = ((Enum) value).name();
                } else {
                    valueToInsert = (String) value;
                }
            }
            valueToInsert = StringUtils.trimToEmpty(valueToInsert);
            ps.setString(index, valueToInsert);
        } else if (int.class.equals(returnType) || Integer.class.equals(returnType)) {
            int valueToInsert = 0;
            if (value != null) {
                valueToInsert = (Integer) value;
            }
            ps.setInt(index, valueToInsert);
        } else if (long.class.equals(returnType) || Long.class.equals(returnType)) {
            long valueToInsert = 0L;
            if (value != null) {
                valueToInsert = (Long) value;
            }
            ps.setLong(index, valueToInsert);
        } else if (double.class.equals(returnType) || Double.class.equals(returnType)) {
            double valueToInsert = 0.0;
            if (value != null) {
                valueToInsert = (Double) value;
            }
            ps.setDouble(index, valueToInsert);
        } else if (short.class.equals(returnType) || Short.class.equals(returnType)) {
            short valueToInsert = 0;
            if (value != null) {
                valueToInsert = (Short) value;
            }
            ps.setShort(index, valueToInsert);
        } else if (boolean.class.equals(returnType) || Boolean.class.equals(returnType)) {
            boolean valueToInsert = false;
            if (value != null) {
                valueToInsert = (Boolean) value;
            }
            ps.setBoolean(index, valueToInsert);
        } else if (LocalDate.class.equals(returnType)) {
            Date valueToInsert = null;
            if (value != null) {
                valueToInsert = Date.valueOf((LocalDate) value);
            }
            ps.setDate(index, valueToInsert);
        } else if (LocalDateTime.class.equals(returnType)) {
            Timestamp valueToInsert = null;
            if (value != null) {
                valueToInsert = Timestamp.valueOf((LocalDateTime) value);
            }
            ps.setTimestamp(index, valueToInsert);
        } else {
            ps.setObject(index, value);
        }
    }

    private String normalizeTableName(String tableName) {
        return tableName.contains(".") ? tableName.split("\\.")[1] : tableName;
    }
}
