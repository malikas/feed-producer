package com.pcb.feedproducer.cdcevent.persistence.exception;

public class CdcEventPersistenceException extends RuntimeException {
    public CdcEventPersistenceException(String message) {
        super(message);
    }

    public CdcEventPersistenceException(Throwable cause) {
        super(cause);
    }

    public CdcEventPersistenceException(String message, Throwable cause) {
        super(message, cause);
    }
}
