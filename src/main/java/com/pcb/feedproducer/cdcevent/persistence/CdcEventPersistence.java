package com.pcb.feedproducer.cdcevent.persistence;

import com.pcb.feedproducer.cdcevent.model.CdcEvent;
import com.pcb.feedproducer.cdcevent.model.CdcEventEnvelope;
import com.pcb.feedproducer.feed.model.FeedDetails;

import java.util.List;

public interface CdcEventPersistence {
    void persistCdcEvent(CdcEventEnvelope<? extends CdcEvent<?>> cdcEventEnvelope);

    List<FeedDetails> fetchFeedDetails(CdcEventEnvelope<? extends CdcEvent<?>> cdcEventEnvelope);
}
