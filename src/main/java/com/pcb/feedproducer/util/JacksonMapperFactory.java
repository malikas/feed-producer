package com.pcb.feedproducer.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.experimental.UtilityClass;

import java.util.Random;
import java.util.TimeZone;

@UtilityClass
public class JacksonMapperFactory {
    private static final int POOL_SIZE = 5;
    private static final ObjectMapper[] MAPPERS;
    private static final Random RANDOM_GENERATOR;

    static {
        MAPPERS = new ObjectMapper[POOL_SIZE];
        for (int i = 0; i < POOL_SIZE; i++) {
            MAPPERS[i] = new ObjectMapper()
                    .setTimeZone(TimeZone.getDefault())
                    .registerModule(new JavaTimeModule())
                    .setSerializationInclusion(JsonInclude.Include.NON_NULL)
                    .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
                    .enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        }
        RANDOM_GENERATOR = new Random();
    }

    public static ObjectMapper getMapper() {
        return MAPPERS[RANDOM_GENERATOR.nextInt(POOL_SIZE)];
    }
}
