package com.pcb.feedproducer.util;

import com.google.common.base.Preconditions;
import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class LocalEventBus {
    private static final int DEFAULT_EXECUTOR_SIZE = 32;

    private static ExecutorService executor;

    private static EventBus eventBus;
    private static AsyncEventBus asyncEventBus;

    private static final AtomicBoolean configured = new AtomicBoolean(false);

    private LocalEventBus() {
    }

    public static void configure() {
        configure(DEFAULT_EXECUTOR_SIZE);
    }

    public synchronized static void configure(int executorSize) {
        Preconditions.checkArgument(executorSize > 0, "executorSize must be a positive number");

        if (!configured.getAndSet(true)) {
            executor = Executors.newFixedThreadPool(executorSize, new ThreadFactoryBuilder().setNameFormat("LocalEventBus-%d").build());
            eventBus = new EventBus("LocalEventBusSync");
            asyncEventBus = new AsyncEventBus("LocalEventBusAsync", executor);
        }
    }

    public static void register(Object object) {
        if (!configured.get()) {
            configure();
        }
        eventBus.register(object);
        asyncEventBus.register(object);
    }

    public static void unregister(Object object) {
        if (!configured.get()) {
            configure();
        }
        eventBus.unregister(object);
        asyncEventBus.unregister(object);
    }

    public static void post(Object event) {
        if (!configured.get()) {
            configure();
        }
        eventBus.post(event);
    }

    public static void postAsync(Object event) {
        if (!configured.get()) {
            configure();
        }
        asyncEventBus.post(event);
    }

    public static void shutdown() {
        if (executor != null) {
            MoreExecutors.shutdownAndAwaitTermination(executor, 1, TimeUnit.SECONDS);
        }
    }
}
