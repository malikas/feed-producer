SELECT
  a.ACCOUNT_NO as customerAcctNumber,
  a.PRODUCT_UID as productId,
  a.INACTIVE_CYCLES as numberOfCyclesInactive,
  a.OPEN_DT as openDate,
  AM00_LIMIT_CREDIT as limitCredit,
  AM00_STATC_CURRENT_PAST_DUE as statcCurrentPastDue,
  AM00_CUSTOM_DATA_81 as customData81,
  AM00_DATE_LAST_BILLING as dateLastBilling,
  AM00_STATC_CLOSED as statcClosed,
  AM00_STATC_SECURITY_FRAUD as statcSecurityFraud,
  AM00_BANKRUPTCY_TYPE as bankruptcyType,
  AM00_STATC_CREDIT_REVOKED as statcCreditRevoked,
  AM00_STATC_CHARGEOFF as statcChargeoff,
  AM00_STATF_REINSTATE_CHARGEOFF as statfReinstateChargeoff,
  AM00_STATC_CURRENT_OVERLIMIT as statcCurrentOverlimit,
  AM00_STATC_WATCH as statcWatch,
  AM00_STATC_TIERED_AUTH as statcTieredAuth,
  AM00_STATF_CREDIT_COUNSELING as statfCreditCounseling,
  AM00_STATF_CRV as statfCrv,
  AM01_CARD_RECEIPT_VERIFY_IND as cardReceiptVerifyInd,
  AM01_DATE_LAST_PIN_CHANGE as dateLastPinChange,
  AM01_DATE_ACCOUNT_VALID_FROM as dateAccountValidFrom,
  AM01_DATE_LAST_CARD_REQUEST as dateLastCardRequest,
  AM01_REASON_LAST_CARD_REQUEST as reasonLastCardRequest,
  AM02_BALANCE_CURRENT as balanceCurrent,
  AM04_DELINQUENT_AMOUNT as delinquentAmount,
  AM0A_STATC_CARD_WATCH as statcCardWatch,
  AM0E_NAME_EMBOSS as nameEmboss,
  (
    SELECT
      ci.CUSTOMER_IDENTIFIER_NO
    FROM
      ACCOUNT_CUSTOMER ac
      INNER JOIN CUSTOMER c on ac.CUSTOMER_UID = c.CUSTOMER_UID
      INNER JOIN CUSTOMER_IDENTIFIER ci on c.CUSTOMER_UID = ci.CUSTOMER_UID
    WHERE
      ac.ACCOUNT_UID = a.ACCOUNT_UID
      AND ac.ACCOUNT_CUSTOMER_ROLE_UID = 1
      AND ac.ACTIVE_IND = 'Y'
      AND ci.TYPE = 'PCF-CUSTOMER-ID'
      AND ci.DISABLED_IND = 'N'
  ) as customerIdFromHeader,
  (
    SELECT
      ci.CUSTOMER_IDENTIFIER_NO
    FROM
      ACCOUNT_CUSTOMER ac
      INNER JOIN CUSTOMER c on ac.CUSTOMER_UID = c.CUSTOMER_UID
      INNER JOIN CUSTOMER_IDENTIFIER ci on c.CUSTOMER_UID = ci.CUSTOMER_UID
    WHERE
      ac.ACCOUNT_UID = a.ACCOUNT_UID
      AND ac.ACCOUNT_CUSTOMER_ROLE_UID = 2
      AND ac.ACTIVE_IND = 'Y'
      AND ci.TYPE = 'PCF-CUSTOMER-ID'
      AND ci.DISABLED_IND = 'N'
    LIMIT
      1
  ) as jointCustomerId,
  (
    SELECT
      COUNT(*)
    FROM
      ACCESS_MEDIUM am
      INNER JOIN ACCOUNT_CUSTOMER ac on am.ACCOUNT_CUSTOMER_UID = ac.ACCOUNT_CUSTOMER_UID
    WHERE
      ac.ACCOUNT_UID = a.ACCOUNT_UID
      AND (
        am.DEACTIVATED_DT IS NULL
        OR am.DEACTIVATED_DT > NOW()
      )
      AND (
        am.EXPIRY_DT IS NULL
        OR am.EXPIRY_DT > NOW()
      )
  ) as numberOfPaymentIds,
  (
    SELECT
      COUNT(*)
    FROM
      ACCOUNT_CUSTOMER ac
      INNER JOIN CUSTOMER c on ac.CUSTOMER_UID = c.CUSTOMER_UID
      INNER JOIN CUSTOMER_IDENTIFIER ci on c.CUSTOMER_UID = ci.CUSTOMER_UID
    WHERE
      ac.ACCOUNT_UID = a.ACCOUNT_UID
      AND ac.ACCOUNT_CUSTOMER_ROLE_UID = 2
      AND ac.ACTIVE_IND = 'Y'
      AND ci.TYPE = 'PCF-CUSTOMER-ID'
      AND ci.DISABLED_IND = 'N'
  ) as numberOfAuthorizedUsers,
  (
    SELECT
      iar.ACTIVE_IND
    FROM
      INTERAC_AUTODEP_REGISTER iar
    WHERE
      iar.ACCOUNT_UID = a.ACCOUNT_UID
  ) as userIndicator01,
  (
    SELECT
      ai.ACCOUNT_IDENTIFIER_NO
    FROM
      ACCOUNT_IDENTIFIER ai
    WHERE
      ai.ACCOUNT_UID = a.ACCOUNT_UID
      AND ai.TYPE = 'PCB'
    LIMIT
      1
  ) as accountIdentifierNo
FROM
  ACCOUNT a
  INNER JOIN (
    SELECT
      SUBSTRING (MAST_ACCOUNT_ID, 2) as MAST_ACCOUNT_ID,
      AM00_LIMIT_CREDIT,
      AM00_STATC_CURRENT_PAST_DUE,
      AM00_CUSTOM_DATA_81,
      AM00_DATE_LAST_BILLING,
      AM00_STATC_CLOSED,
      AM00_STATC_SECURITY_FRAUD,
      AM00_BANKRUPTCY_TYPE,
      AM00_STATC_CREDIT_REVOKED,
      AM00_STATC_CHARGEOFF,
      AM00_STATF_REINSTATE_CHARGEOFF,
      AM00_STATC_CURRENT_OVERLIMIT,
      AM00_STATC_WATCH,
      AM00_STATC_TIERED_AUTH,
      AM00_STATF_CREDIT_COUNSELING,
      AM00_STATF_CRV
    FROM
      TSYS_PCB_MC_ACCTMASTER_DAILY_AM00
  ) as am00 ON am00.MAST_ACCOUNT_ID = a.ACCOUNT_NO
  INNER JOIN (
    SELECT
      SUBSTRING (MAST_ACCOUNT_ID, 2) as MAST_ACCOUNT_ID,
      AM01_CUSTOMER_ID,
      AM01_ACCOUNT_NUM,
      AM01_CARD_RECEIPT_VERIFY_IND,
      AM01_DATE_LAST_PIN_CHANGE,
      AM01_DATE_ACCOUNT_VALID_FROM,
      AM01_DATE_LAST_CARD_REQUEST,
      AM01_REASON_LAST_CARD_REQUEST
    FROM
      TSYS_PCB_MC_ACCTMASTER_DAILY_AM01
  ) as am01 ON am01.MAST_ACCOUNT_ID = a.ACCOUNT_NO
  INNER JOIN (
    SELECT
      SUBSTRING (MAST_ACCOUNT_ID, 2) as MAST_ACCOUNT_ID,
      AM02_BALANCE_CURRENT
    FROM
      TSYS_PCB_MC_ACCTMASTER_DAILY_AM02
  ) as am02 ON am02.MAST_ACCOUNT_ID = a.ACCOUNT_NO
  INNER JOIN (
    SELECT
      SUBSTRING (MAST_ACCOUNT_ID, 2) as MAST_ACCOUNT_ID,
      SUM (
        CAST (AM04_AMT_PASTDUE_1 AS DECIMAL) + CAST (AM04_AMT_PASTDUE_2 AS DECIMAL) + CAST (AM04_AMT_PASTDUE_3 AS DECIMAL) + CAST (AM04_AMT_PASTDUE_4 AS DECIMAL) + CAST (AM04_AMT_PASTDUE_5 AS DECIMAL) + CAST (AM04_AMT_PASTDUE_6 AS DECIMAL) + CAST (AM04_AMT_PASTDUE_7 AS DECIMAL) + CAST (AM04_AMT_PASTDUE_8 AS DECIMAL) + CAST (AM04_AMT_PASTDUE_9 AS DECIMAL) + CAST (AM04_AMT_PASTDUE_10 AS DECIMAL) + CAST (AM04_AMT_PASTDUE_11 AS DECIMAL) + CAST (AM04_AMT_PASTDUE_12 AS DECIMAL)
      ) as AM04_DELINQUENT_AMOUNT
    FROM
      TSYS_PCB_MC_ACCTMASTER_DAILY_AM04
    GROUP BY
      MAST_ACCOUNT_ID
  ) as am04 ON am04.MAST_ACCOUNT_ID = a.ACCOUNT_NO
  INNER JOIN (
    SELECT
      SUBSTRING (MAST_ACCOUNT_ID, 2) as MAST_ACCOUNT_ID,
      AM0A_STATC_CARD_WATCH
    FROM
      TSYS_PCB_MC_ACCTMASTER_DAILY_AM0A
  ) as am0a ON am0a.MAST_ACCOUNT_ID = a.ACCOUNT_NO
  INNER JOIN (
    SELECT
      SUBSTRING (MAST_ACCOUNT_ID, 2) as MAST_ACCOUNT_ID,
      AM0E_NAME_EMBOSS
    FROM
      TSYS_PCB_MC_ACCTMASTER_DAILY_AM0E
  ) as am0e ON am0e.MAST_ACCOUNT_ID = a.ACCOUNT_NO
  INNER JOIN ACCOUNT_CUSTOMER ac ON a.ACCOUNT_UID = ac.ACCOUNT_UID
  INNER JOIN CUSTOMER c ON c.CUSTOMER_UID = ac.CUSTOMER_UID
WHERE
  c.CUSTOMER_UID = 32093
  AND ac.ACCOUNT_CUSTOMER_ROLE_UID = 1
  AND ac.ACTIVE_IND = 'Y'
  AND (
    a.PRODUCT_UID IN (7, 8, 9, 10)
    OR (
      SELECT
        COUNT (*)
      FROM
        ACCOUNT a1
        INNER JOIN ACCOUNT_CUSTOMER ac1 ON ac1.CUSTOMER_UID = ac.CUSTOMER_UID
      WHERE
        a1.PRODUCT_UID IN (7, 8, 9, 10)
    ) > 0
  )
GROUP BY
  customerAcctNumber,
  productId,
  numberOfCyclesInactive,
  openDate,
  limitCredit,
  statcCurrentPastDue,
  customData81,
  dateLastBilling,
  statcClosed,
  statcSecurityFraud,
  bankruptcyType,
  statcCreditRevoked,
  statcChargeoff,
  statfReinstateChargeoff,
  statcCurrentOverlimit,
  statcWatch,
  statcTieredAuth,
  statfCreditCounseling,
  statfCrv,
  cardReceiptVerifyInd,
  dateLastPinChange,
  dateAccountValidFrom,
  dateLastCardRequest,
  reasonLastCardRequest,
  balanceCurrent,
  delinquentAmount,
  statcCardWatch,
  nameEmboss,
  customerIdFromHeader,
  jointCustomerId,
  numberOfPaymentIds,
  numberOfAuthorizedUsers,
  userIndicator01,
  accountIdentifierNo;